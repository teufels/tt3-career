<?php

declare(strict_types=1);

$iconList = [];
foreach (['tt3career_careershow_icon' => 'plugin_careershow.svg',
     'tt3career_careerlist_icon' => 'plugin_careerlist.svg',
     'tt3career_careerwatchlist_icon' => 'plugin_careerwatchlist.svg',
     'tt3career_careerwatchlistbutton_icon' => 'plugin_careerwatchlistbutton.svg',
 ] as $identifier => $path) {
    $iconList[$identifier] = [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:tt3_career/Resources/Public/Icons/' . $path,
    ];
}

return $iconList;
