<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Core\DependencyInjection;
use Teufels\Tt3Career\Hooks\ItemsProcFunc;

return function (ContainerConfigurator $container, ContainerBuilder $containerBuilder) {
    $containerBuilder->registerForAutoconfiguration(ItemsProcFunc::class)->addTag('tt3_career.ItemsProcFunc');
    $containerBuilder->addCompilerPass(new DependencyInjection\SingletonPass('tt3_career.ItemsProcFunc'));
};
