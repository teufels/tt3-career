<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs',
        'label' => 'backend_title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'backend_title,title,subtitle,description,employmenttype,identifier_name,identifier_value,hiringorganization_name,hiringorganization_website,joblocation_address_street,joblocation_address_postalcode,joblocation_address_city,joblocation_address_region,joblocation_address_country,basesalary_currency,basesalary_unit,remotejob_location_requirements',
        'iconfile' => 'EXT:tt3_career/Resources/Public/Icons/tx_tt3career_domain_model_googleforjobs.svg',
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, subtitle, description, employmenttype, identifier_name, identifier_value, hiringorganization_name, hiringorganization_website, hiringorganization_logo, joblocation_address_street, joblocation_address_postalcode, joblocation_address_city, joblocation_address_region, joblocation_address_country, basesalary_currency, basesalary_unit, basesalary_value, remotejob, remotejob_location_requirements, date_posted, valid_through',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, subtitle, description, employmenttype, identifier_name, identifier_value, hiringorganization_name, hiringorganization_website, hiringorganization_logo, joblocation_address_street, joblocation_address_postalcode, joblocation_address_city, joblocation_address_region, joblocation_address_country, basesalary_currency, basesalary_unit, basesalary_value, remotejob, remotejob_location_requirements, date_posted, valid_through, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_tt3career_domain_model_googleforjobs',
                'foreign_table_where' => 'AND {#tx_tt3career_domain_model_googleforjobs}.{#pid}=###CURRENT_PID### AND {#tx_tt3career_domain_model_googleforjobs}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'backend_title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.backend_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'default' => ''
            ],
        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'subtitle' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'employmenttype' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.employmenttype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'identifier_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.identifier_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'identifier_value' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.identifier_value',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'hiringorganization_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.hiringorganization_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'hiringorganization_website' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.hiringorganization_website',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'hiringorganization_logo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.hiringorganization_logo',
            'config' => [
                'type' => 'file',
                'maxitems' => 1,
                'allowed' => 'common-image-types',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],

        ],
        'joblocation_address_street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.joblocation_address_street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'joblocation_address_postalcode' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.joblocation_address_postalcode',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'joblocation_address_city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.joblocation_address_city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'joblocation_address_region' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.joblocation_address_region',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'joblocation_address_country' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.joblocation_address_country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'basesalary_currency' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.basesalary_currency',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'basesalary_unit' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.basesalary_unit',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'basesalary_value' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.basesalary_value',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ]
        ],
        'remotejob' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.remotejob',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ]
                ],
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ]
        ],
        'remotejob_location_requirements' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.remotejob_location_requirements',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'date_posted' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.date_posted',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'valid_through' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_googleforjobs.valid_through',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],

        'joboffer' => [
            'config' => [
                'type' => 'passthrough',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
    ],
];
