<?php
defined('TYPO3') or die();

$extensionKey = 'tt3_career';
$extensionTitle = '[ṯeufels] Career';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);