<?php
defined('TYPO3') or die();

$extensionName = strtolower('tt3_career');
$pluginNames = ['Careershow', 'Careerlist', 'Careerwatchlist', 'Careerwatchlistbutton'];

foreach ($pluginNames as $pluginName) {
    $pluginSignature = str_replace('_', '', $extensionName) . '_' . str_replace('_', '', strtolower($pluginName));
    $pluginIcon = $pluginSignature . '_icon';

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Tt3Career',
        $pluginName,
        'LLL:EXT:'. $extensionName . '/Resources/Private/Language/locallang_db.xlf:tx_' . $pluginSignature . '.name',
        $pluginIcon,
    );

    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$pluginSignature] = $pluginIcon;

    $GLOBALS['TCA']['tt_content']['types']['list']['previewRenderer'] = \Teufels\Tt3Career\Preview\PreviewRenderer::class;
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,pages,recursive';
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        $pluginSignature,
        'FILE:EXT:'. $extensionName . '/Configuration/FlexForms/flexform_' . strtolower($pluginName) . '.xml'
    );
}