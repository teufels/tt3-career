<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer',
        'label' => 'backend_title',
        'label_alt' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'backend_title,title,subtitle,meta_title,meta_description,meta_keywords,og_title,og_description,twitter_title,twitter_description,twitter_card,header_text',
        'iconfile' => 'EXT:tt3_career/Resources/Public/Icons/tx_tt3career_domain_model_joboffer.svg',
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, header_image, header_text, title, subtitle, path_segment, start_date, employmenttype, company, location, intro, task, profile, benefits, information, download, external_link, contact, additional_receiver, meta_title, meta_description, meta_keywords, og_title, og_description, og_image, twitter_title, twitter_description, twitter_image, twitter_card, googleforjobs',
    ],
    'types' => [
        '1' => [
            'showitem' => '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    backend_title, 
                    --palette--;;palette_title,
                    path_segment,
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer,
                    --palette--;;palette_joboffer_start,
                    --palette--;;palette_joboffer,
                    --palette--;;palette_joboffer_location,
                    --palette--;;palette_joboffer_company,
                    --palette--;;palette_joboffer_link, download,
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.content, 
                    intro, task, profile, benefits, information,
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.contact, 
                    contact, additional_receiver,
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.header_image, 
                    header_image, header_text, 
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.seo, 
                    --palette--;;palette_meta,
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.opengraph, 
                    --palette--;;palette_opengraph_og,
                    --palette--;;palette_opengraph_twitter,
                --div--;LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.googleforjobs, 
                    googleforjobs,    
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                    categories,     
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    sys_language_uid, l10n_parent, l10n_diffsource,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;palette_access,
            ',
        ],
    ],
    'palettes' => [
        'palette_title' => [
            'showitem' => 'title, subtitle',
        ],
        'palette_joboffer_start' => [
            'showitem' => 'start_date',
        ],
        'palette_joboffer' => [
            'showitem' => 'employmenttype, career_level',
        ],
        'palette_joboffer_location' => [
            'showitem' => 'location, country',
        ],
        'palette_joboffer_company' => [
            'showitem' => 'company, department',
        ],
        'palette_joboffer_link' => [
            'showitem' => 'external_link',
        ],
        'palette_meta' => [
            'showitem' => 'meta_title, --linebreak--, meta_description, meta_keywords',
        ],
        'palette_opengraph_og' => [
            'label' => 'Facebook',
            'showitem' => 'og_title, --linebreak--, og_description, --linebreak--, og_image,',
        ],
        'palette_opengraph_twitter' => [
            'label' => 'X/Twitter',
            'showitem' => 'twitter_title, --linebreak--, twitter_description, --linebreak--, twitter_image, --linebreak--, twitter_card,',
        ],
        'palette_access' => [
            'showitem' => 'hidden, --linebreak--, starttime, endtime,',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_tt3career_domain_model_joboffer',
                'foreign_table_where' => 'AND {#tx_tt3career_domain_model_joboffer}.{#pid}=###CURRENT_PID### AND {#tx_tt3career_domain_model_joboffer}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'backend_title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.backend_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'default' => ''
            ],
        ],
        'subtitle' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'path_segment' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.path_segment',
            'type' => 'slug',
            'config' => [
                'type' => 'slug',
                'size' => 50,
                'generatorOptions' => [
                    'fields' => [
                        'title'
                    ],
                    'fieldSeparator' => '-',
                    'replacements' => [
                        '/' => '-',
                    ],
                ],
                'fallbackCharacter' => '-',
                'eval' => 'unique',
            ],
        ],
        'header_image' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.header_image',
            'config' => [
                'type' => 'file',
                'maxitems' => 1,
                'allowed' => 'common-image-types',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'header_text' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.header_text',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'teufels_presets',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'start_date' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.start_date',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'employmenttype' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.employmenttype',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingleBox',
                'items' => [
                    ['Full-time', 'FULL_TIME'],
                    ['Part-time', 'PART_TIME'],
                    ['Contractor', 'CONTRACTOR'],
                    ['Temporary', 'TEMPORARY'],
                    ['Intern', 'INTERN'],
                    ['Volunteer', 'VOLUNTEER'],
                    ['Per diem', 'PER_DIEM'],
                    ['Other', 'OTHER'],
                ],
                'size' => 1,
                'eval' => 'required',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'career_level' => [
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.career_level',
            'config' => [
                'type' => 'input',
                'size' => 40,
                'eval' => 'trim',
                'valuePicker' => [
                    'items' => [
                        [
                            'Entry-Level',
                            'CAREER_STARTERS',
                        ],
                        [
                            'Young professionals',
                            'YOUNG_PROFESSIONALS',
                        ],
                        [
                            'Experienced Professional',
                            'PROFESSIONALS',
                        ],
                        [
                            'Executive',
                            'EXECUTIVE',
                        ],
                        [
                            'Pupils',
                            'PUPILS',
                        ],
                        [
                            'Students',
                            'STUDENTS',
                        ],
                        [
                            'Other',
                            'OTHER',
                        ],
                    ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'department' => [
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.department',
            'config' => [
                'type' => 'input',
                'size' => 40,
                'eval' => 'trim',
                'valuePicker' => [
                    'items' => [
                        [
                            'ADMINISTRATION',
                            'Administration'
                        ],
                        [
                            'CONSULTING',
                            'Consulting'
                        ],
                        [
                            'FACILITY_MANAGEMENT',
                            'Facility Management'
                        ],
                        [
                            'FINANCE_ACCOUNTING_CONTROLLING',
                            'Finance, Accounting & Controlling'
                        ],
                        [
                            'HUMAN_RESOURCES',
                            'Human Resources'
                        ],
                        [
                            'INDUSTRIAL_ENGINEERING',
                            'Industrial Engineering'
                        ],
                        [
                            'IT',
                            'IT'
                        ],
                        [
                            'LEGAL_AFFAIRS',
                            'Legal Affairs'
                        ],
                        [
                            'LOGISTICS',
                            'Logistics'
                        ],
                        [
                            'MARKETING',
                            'Marketing'
                        ],
                        [
                            'MANAGEMENT_EXECUTIVE',
                            'Management/Executive'
                        ],
                        [
                            'MAINTENANCE',
                            'Maintenance'
                        ],
                        [
                            'PRODUCTION',
                            'Production'
                        ],
                        [
                            'PRODUCT_MANAGEMENT',
                            'Product Management'
                        ],
                        [
                            'PURCHASING',
                            'Purchasing'
                        ],
                        [
                            'SALES',
                            'Sales'
                        ],
                        [
                            'QUALITY_MANAGEMENT',
                            'Quality Management'
                        ],
                        [
                            'RESEARCH_DEVELOPMENT',
                            'Research and Development'
                        ],
                        [
                            'OTHER',
                            'Other'
                        ]
                       
                    ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'company' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.company',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'location' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.location',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'additional_locations' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.additional_locations',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'country' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'intro' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.intro',
            'description' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.intro.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'teufels_presets',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ],

        ],
        'task' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.task',
            'description' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.task.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'teufels_presets',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'profile' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.profile',
            'description' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.profile.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'teufels_presets',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'benefits' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.benefits',
            'description' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.benefits.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'teufels_presets',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'information' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.information',
            'description' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.information.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'teufels_presets',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'download' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.download',
            'config' => [
                'type' => 'file',
                'maxitems' => 1,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'external_link' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.external_link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'meta_title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.meta_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'meta_description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.meta_description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'default' => ''
            ]
        ],
        'meta_keywords' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.meta_keywords',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'default' => ''
            ]
        ],
        'og_title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.og_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'og_description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.og_description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'default' => ''
            ]
        ],
        'og_image' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.og_image',
            'config' => [
                'type' => 'file',
                'maxitems' => 1,
                'allowed' => 'common-image-types',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],

        ],
        'twitter_title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.twitter_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'twitter_description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.twitter_description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'default' => ''
            ]
        ],
        'twitter_image' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.twitter_image',
            'config' => [
                'type' => 'file',
                'maxitems' => 1,
                'allowed' => 'common-image-types',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'twitter_card' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.twitter_card',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 'summary',
                'items' => [
                    ['Summary Card', 'summary'],
                    ['Summary Card with a large image', 'summary_large_image'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'required',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'googleforjobs' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.googleforjobs',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_tt3career_domain_model_googleforjobs',
                'foreign_field' => 'joboffer',
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'contact' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.contact',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tt_address',
                'foreign_table_where' => 'AND tt_address.sys_language_uid IN (-1, 0)',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 1,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ]
            ],
        ],
        'additional_receiver' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.additional_receiver',
            'description' => 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_db.xlf:tx_tt3career_domain_model_joboffer.additional_receiver.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'max' => 255,
                'softref' => 'email',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'categories' => [
            'config' => [
                'type' => 'category'
            ]
        ]
    ],
];
