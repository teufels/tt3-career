<?php
defined('TYPO3') or die();

call_user_func(function() {

    /**
     * configure Plugin
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Tt3Career',
        'Careerlist',
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'list'],
        // non-cacheable actions
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'list']
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Tt3Career',
        'Careershow',
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'list, show, perform, addToWatchlist'],
        // non-cacheable actions
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'show, perform, addToWatchlist']
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Tt3Career',
        'Careerwatchlist',
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'watchlist, addToWatchlist'],
        // non-cacheable actions
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'watchlist, addToWatchlist']
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Tt3Career',
        'CareerwatchlistButton',
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'watchlistButton'],
        // non-cacheable actions
        [Teufels\Tt3Career\Controller\JobOfferController::class => 'watchlistButton']
    );

    /**
     * Load PageTS
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
        @import \'EXT:tt3_career/Configuration/TsConfig/Page/BackendPreview.tsconfig\'
        @import \'EXT:tt3_career/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig\'
        @import \'EXT:tt3_career/Configuration/TsConfig/Page/Settings.tsconfig\'
    ');

    /**
     * Backend CSS Styling
     */
    $GLOBALS['TYPO3_CONF_VARS']['BE']['stylesheets']['tt3_career'] = 'EXT:tt3_career/Resources/Public/Css/Backend/';

    /**
    * Cache
    */
    if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tt3career']) || !is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tt3career']))
    {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tt3career'] = [];
    }

    // Filter requests are not cached, so these parameters don't need to influence the cache hash
    $GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = '^tx_tt3career_careerlist[constraint]';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = '^tx_tt3career_careerlist[__referrer]';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = '^tx_tt3career_careerlist[__trustedProperties]';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_tt3career_careerlist[submit]';
});