$(function () {
    //general configuration
    const options = {
        plugins : ['remove_button'],
        selectOnTab: false
    };
    $('select:not(#sword)').selectize(options);

    //sword configuration
    $('#sword').selectize({
        plugins : ['remove_button'],
        create: true,
        persist: false,
        createOnBlur: true,
        openOnFocus: false,
        maxItems: 1
    });
});