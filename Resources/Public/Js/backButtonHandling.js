function goBack() {
    sessionStorage.setItem('scrollToAnchor', 'tx-tt3-career');
    const listPageUrl = sessionStorage.getItem('listPageUrl');
    if (listPageUrl) {
        window.location.href = listPageUrl;
    } else {
        const previousUrl = document.referrer;
        const currentDomain = window.location.hostname;

        if (previousUrl && new URL(previousUrl).hostname === currentDomain) {
            window.history.back();
        } else {
            const currentUrl = window.location.pathname;
            const parentUrl = currentUrl.substring(0, currentUrl.lastIndexOf('/'));

            if (parentUrl) {
                window.location.href = parentUrl;
            } else {
                window.location.href = '/';
            }
        }
    }
}