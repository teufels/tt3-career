{namespace formvh=TYPO3\CMS\Form\ViewHelpers}
{namespace formmailtext=Kitzberger\FormMailtext\ViewHelpers}
<f:layout name="SystemEmail" />
<f:section name="Title">{title}</f:section>
<f:section name="Main">
<formmailtext:renderMessage message="{message -> f:format.stripTags()}">
<formvh:renderAllFormValues renderable="{form.formDefinition}" as="formValue"><f:spaceless>

    <f:variable name="shortlabel" value="{formvh:translateElementProperty(element: formValue.element, property: 'shortlabel') -> f:format.stripTags(allowedTags: '<a>')}" />
    <f:variable name="label" value="{formvh:translateElementProperty(element: formValue.element, property: 'label') -> f:format.stripTags(allowedTags: '<a>')}" />

    <f:if condition="{formValue.isMultiValue}">
        <f:then>
            <f:if condition="{formValue.isSection}"><f:then>*** <formvh:translateElementProperty element="{formValue.element}" property="label" /> ***</f:then><f:else>{f:if(condition: '{shortlabel}', then: '{shortlabel}', else: '{label}')}: <f:for each="{formValue.processedValue}" as="singleValue">- {singleValue}
            </f:for></f:else></f:if>
        </f:then>
        <f:else>
            <f:if condition="{formValue.isSection}"><f:then>*** <formvh:translateElementProperty element="{formValue.element}" property="label" /> ***</f:then><f:else>{f:if(condition: '{shortlabel}', then: '{shortlabel}', else: '{label}')}: <f:if condition="{formValue.processedValue}"><f:then>{formValue.processedValue -> f:format.raw()}</f:then><f:else>-</f:else></f:if></f:else></f:if>
        </f:else>
    </f:if>
</f:spaceless>
</formvh:renderAllFormValues>
</formmailtext:renderMessage>
</f:section>
