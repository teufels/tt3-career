# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

***
## [Unreleased]

### [2.1.0] 2025-01-13
- add new text-field 'benefits'
- add new option 'Executive' to career_level
- Google for Jobs Strucutred Data set 'identifier' to JobOffer ID
- add new field for 'additionalLocations'
- add multiple Locations to Google for jobs structured Data
- change (incorrect) duplicate class "intro" to correct one "info"
***

## [Released]

### [2.0.2] 2024-12-05
- Remove the hreflang for jobOffer in strict mode with no translations

### [2.0.1] 2024-11-29
- add rel=nofollow to watchlist action

### [2.0.0] 2024-11-28

#### [ADDED]
- Documentation
- Access Rights to Flexform Settings
- settings for template used by related jobs listing
- use provided predefine Form from EXT:tt3_career in Basis could be overwritten by override applicationForm.path in TS Constants to own formular path (by default 1:/form_definitions/)
- use of common partial (RenderContent.html) for Template List
- routeEnhancers default configuration as example for usage
- plugin option '(back) to all joboffers' to watchlist
- show, watchlist to non-cachable action (needed for watchlist add/remove)
- list to non-cachable action (needed for filter)
- section to filter action for anchor target
- back-button Handling on usage watchlist
- watchlist cookie lifetime via TypoScript Constants/Setup
- watchlist function
- pre-filter options
- related job offer list for detail
- auto suggestion for free text search filter
- free text search filter
- (FE) filter options
- template layout selection for list & detail plugin
- settings for max records displayed for listing
- Sliding window pagination
- new fields: career_level,department,country
- add job,firstname,lastname,email to formLog BE view & export
- add rel=nofollow to pagination
- add rel=nofollow to watchlist action
- add DisableLanguageMenuProcessor

#### [CHANGED]
- Use metaTitle for page title if is set
- TS Constants for application form path and include setting
- set upload path to 20 to avoid overring default FileMount (10) fileadmin/user_upload/
- plugin options & BE preview
- TCA config

#### [REMOVED]
- TS receiver settings -> not used

### [1.0.0] 2024-08-20
- [MAJOR] tested & set to stable 1.0.0
- [ADDED] re-added multifile-upload

### [0.0.1 - 0.0.5] 2023-09-25 - 2024-07-09
- [BREAKING] inital from [hive_career](https://bitbucket.org/teufels/hive_career/src/) `update/12.4` but without multifile-upload (form-extended not TYPO3 v12 ready yet)
