[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--career-orange.svg)](https://bitbucket.org/teufels/tt3-career/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__career-red.svg)](https://bitbucket.org/teufels/tt3-career/src/main/)
![version](https://img.shields.io/badge/version-2.1.*-yellow.svg?style=flat-square)

[ ṯeufels ] Career
==========
This extension allows you to create and manage job listings, featuring filtering, watchlist, detail view with optional application form or link to external one, and compatibility with Google for Jobs structured data.

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req teufels/tt3-career`

#### Links
|                    | URL                                                            |
|--------------------|----------------------------------------------------------------|
| **Repository:**    | https://bitbucket.org/teufels/tt3-career/                      |
| **Packagist:**     | https://packagist.org/packages/teufels/tt3-career/             |
| **TER:**           | https://extensions.typo3.org/package/teufels/tt3-career        |
| **Documentation:** | http://localhost:5173/Documentation-GENERATED-temp/Index.html  |

***

## 1. Features
- Google for Jobs Structured Data
- Application Form with Multi-File-Upload in detail view. Predefined form provided by this extension.
- Filtering Options after search word (title, subtitle, keywords), employment type, career level, department, location, country
- Pre-Filter for employment type, career level, department, location, country
- Watchlist
- Related Jobs

***

## 2. Installation
The recommended way to install the extension is by using Composer.
In your Composer based TYPO3 project root, just do `composer req teufels/tt3-career`.

***

## 3. Changelog
Read on [Bitbucket Changelog.md](https://bitbucket.org/teufels/tt3-career/src/main/CHANGELOG.md)

***

## 4. Documentation
[Documentation](http://localhost:5173/Documentation-GENERATED-temp/Index.html) provided with this extension.

[Rendering the Documentation folder locally with Docker](https://docs.typo3.org/m/typo3/docs-how-to-document/main/en-us/Howto/RenderingDocs/Index.html#rendering-the-file-documentation-folder-locally-with-docker)

### 4.1 Additional Documentation
- https://teufelsgmbh.sharepoint.com/sites/TELEKOLLEGDigital/SitePages/Forms---Bewerbungsformular-(hive_career).aspx?web=1
- https://teufelsgmbh.sharepoint.com/sites/TELEKOLLEGDigital/SitePages/Forms.aspx
- https://teufelsgmbh.sharepoint.com/sites/TELEKOLLEGDigital/SitePages/Google-for-Jobs(1).aspx?web=1

***

## 5. Dependencies
### 5.1 Requirements
- `"typo3/cms-form": "^12.4"`
- `"friendsoftypo3/tt-address": "^9.0"`
- `"beewilly/form-element-linked-text": "^2.0"`
- `"pagemachine/typo3-formlog": "^2.2"`
- `"kitzberger/form-mailtext": "^2.1"`
- `"teufels/tt3-scheduler": "^1.0"`
- `"beechit/fal-securedownload": "^5.0"`
- `"teufels/tt3-form-extended": "^1.0"`

### 5.2 Suggestion
- `"teufels/tt3-form-post": "^1.0"`
  This TYPO3 extension adds a custom form finisher "HTTP POST/GET" to the
  TYPO3 form framework which call plain HTTP Request (POST/GET) to transfer data via cURL.
  The transmitted Data will be generated as array from the Form Fields.
- `"teufels/tt3-career-importer": "dev-main"`
  import job offers from external sources with the external_import Extension
- `"t3brightside/advancedtitle": "^1.3"`
  This extension adds page title prefix, suffix and absolute title field to the SEO tab.
  And is used to output the record title as page title on detail pages.
  It is included in the [teufels] sitepackage

### 5.3 Other
- Selectize v0.15.2 [(Github)](https://github.com/selectize/selectize.js)

***

## 6. How to use
Quick start guide read on [Quick start](http://localhost:5173/Documentation-GENERATED-temp/QuickStart/Index.html)

### 6.1 Using custom template
- create TT3Career.tsconfig in sitepackage & import the file in All.tscconfig
- add own Templates in tx_tt3career.settings.templateLayouts.list or add own Templates in tx_tt3career.settings.templateLayouts.detail
- Example:
  ```
  tx_tt3career.settings {
    templateLayouts {
      list {
        custom = Custom List Layout
      }
      detail {
        custom = Custom Detail Layout
      }
    }
  }
  ```
- remove basis Templates with `>` in TT3Career.tsconfig (if it should not be selectable in the backend)
  - Example
  ```
  tx_tt3career.settings {
      templateLayouts {
        list >
        list {
          only = only available Template Layout
        }
      }
  }
  ```

### 6.2 Enable filter for frontend use
- in the plugin you can select the filters that are used in the frontend. The order is taken into account

### 6.3 Remove available filter
- create TT3Career.tsconfig in sitepackage & import the file in All.tscconfig
- remove basis filter  with `>` in TT3Career.tsconfig (if it should not be selectable in the backend)
  - Example
  ```
  tx_tt3career.settings {
      filter {
        emplyomentType >
      }
  }
  ```

### 6.4 Modify aviablable filter
- the label and relationType could be overwritten, but it is not possible to create your own filters

### 6.5 Usage of application form
- enable/disable integration of application form in detail page by setting `tx_tt3career.settings.applicationForm.include` to `true` or `false` in TS Constants
- this extension provides a predefined form (`EXT:tt3_career/Resources/Private/Form/BasicApplication.form.yaml`) which could be used.
  Own form could be used by changing `tx_tt3career.settings.applicationForm.path`
  - Example:
  ```
  tx_tt3career.settings.applicationForm.path = 1:/form_definitions/applicationFormCustom.form.yaml
  ```

***

## 7. Update & Migration from hive_career
see [Migration Documentation](http://localhost:5173/Documentation-GENERATED-temp/Maintenance/Migration.html#) provided with this extension.

***

This extension is provided by teufels GmbH © 2024
