<?php

declare(strict_types=1);

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 ***/

namespace Teufels\Tt3Career\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3careerDataUpdater')]
class DataUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Career: Migrate data';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all data from tx_hivecareer_domain_model_joboffer to the new tx_tt3career_domain_model_joboffer';
        if($this->checkMigrationTableExist('tx_hivecareer_domain_model_joboffer')) { $description .= ': ' . count($this->getMigrationRecords('tx_hivecareer_domain_model_joboffer')); }
        $description .= ' and all data from tx_hivecareer_domain_model_googleforjobs to the new tx_tt3career_domain_model_googleforjobs';
        if($this->checkMigrationTableExist('tx_hivecareer_domain_model_googleforjobs')) { $description .= ': ' . count($this->getMigrationRecords('tx_hivecareer_domain_model_googleforjobs')); }

        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return $this->checkMigrationTableExist('tx_hivecareer_domain_model_joboffer');
    }

    public function performMigration(): bool
    {
        //data joboffer
        $sql = "INSERT INTO tx_tt3career_domain_model_joboffer 
        SELECT *
        FROM tx_hivecareer_domain_model_joboffer WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3barometer_domain_model_barometer');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();

        //data googleforjobs
        $sql = "INSERT INTO tx_tt3career_domain_model_googleforjobs
        SELECT *
        FROM tx_hivecareer_domain_model_googleforjobs WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3career_domain_model_googleforjobs');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();

        return true;
    }

    protected function getMigrationRecords(string $tablename): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable($tablename);
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid')
            ->from($tablename)
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function checkMigrationTableExist(string $tablename): bool {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable($tablename)
            ->getSchemaManager()
            ->tablesExist([$tablename]);
    }

}
