<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3-career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Career\Seo;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Site\Entity\SiteInterface;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Check if a jobOffer record is available
 */
class JobOfferAvailability
{
    public function check(int $languageId, int $jobOfferId = 0): bool
    {
        // get it from current request
        if ($jobOfferId === 0) {
            $jobOfferId = $this->getJobOfferIdFromRequest();
        }
        if ($jobOfferId === 0) {
            throw new \UnexpectedValueException('No jobOffer id provided', 1586431984);
        }

        /** @var SiteInterface $site */
        $site = $this->getRequest()->getAttribute('site');
        $allAvailableLanguagesOfSite = $site->getAllLanguages();

        $targetLanguage = $this->getLanguageFromAllLanguages($allAvailableLanguagesOfSite, $languageId);
        if (!$targetLanguage) {
            throw new \UnexpectedValueException('Target language could not be found', 1586431985);
        }
        return $this->mustBeIncluded($jobOfferId, $targetLanguage);
    }

    protected function mustBeIncluded(int $jobOfferId, SiteLanguage $language): bool
    {
        if ($language->getFallbackType() === 'strict') {
            // @extensionScannerIgnoreLine
            $jobOfferRecord = $this->getJobOfferRecord($jobOfferId, $language->getLanguageId());

            if (!is_array($jobOfferRecord) || empty($jobOfferRecord)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param SiteLanguage[] $allLanguages
     */
    protected function getLanguageFromAllLanguages(array $allLanguages, int $languageId): ?SiteLanguage
    {
        foreach ($allLanguages as $siteLanguage) {
            if ($siteLanguage->getLanguageId() === $languageId) {
                return $siteLanguage;
            }
        }
        return null;
    }

    protected function getJobOfferRecord(int $jobOfferId, int $language)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_tt3career_domain_model_joboffer');
        if ($language === 0) {
            $where = [
                $queryBuilder->expr()->or(
                    $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter($language, Connection::PARAM_INT)),
                    $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter(-1, Connection::PARAM_INT))
                ),
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobOfferId, Connection::PARAM_INT)),
            ];
        } else {
            $where = [
                $queryBuilder->expr()->or(
                    $queryBuilder->expr()->and(
                        $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter(-1, Connection::PARAM_INT)),
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobOfferId, Connection::PARAM_INT))
                    ),
                    $queryBuilder->expr()->and(
                        $queryBuilder->expr()->eq('l10n_parent', $queryBuilder->createNamedParameter($jobOfferId, Connection::PARAM_INT)),
                        $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter($language, Connection::PARAM_INT))
                    ),
                    $queryBuilder->expr()->and(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobOfferId, Connection::PARAM_INT)),
                        $queryBuilder->expr()->eq('l10n_parent', $queryBuilder->createNamedParameter(0, Connection::PARAM_INT)),
                        $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter($language, Connection::PARAM_INT))
                    )
                ),
            ];
        }

        $row = $queryBuilder
            ->select('uid', 'l10n_parent', 'sys_language_uid')
            ->from('tx_tt3career_domain_model_joboffer')
            ->where(...$where)
            ->executeQuery()->fetchAssociative();

        return $row ?: null;
    }

    protected function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }

    public function getJobOfferIdFromRequest(): int
    {
        $jobOfferId = 0;
        /** @var PageArguments $pageArguments */
        $pageArguments = $this->getRequest()->getAttribute('routing');
        if (isset($pageArguments, $pageArguments->getRouteArguments()['tx_tt3career_careershow']['jobOffer'])) {
            $jobOfferId = (int)$pageArguments->getRouteArguments()['tx_tt3career_careershow']['jobOffer'];
        } elseif (isset($this->getRequest()->getQueryParams()['tx_tt3career_careershow']['jobOffer'])) {
            $jobOfferId = (int)$this->getRequest()->getQueryParams()['tx_tt3career_careershow']['jobOffer'];
        }
        return $jobOfferId;
    }
}
