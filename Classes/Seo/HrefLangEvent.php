<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3-career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Career\Seo;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\DataProcessing\LanguageMenuProcessor;
use TYPO3\CMS\Frontend\Event\ModifyHrefLangTagsEvent;

/**
 * Remove the hreflang for jobOffer in strict mode with no translations
 */
class HrefLangEvent
{
    /** @var ContentObjectRenderer */
    protected $cObj;

    protected LanguageMenuProcessor $languageMenuProcessor;

    public function __construct(ContentObjectRenderer $cObj, LanguageMenuProcessor $languageMenuProcessor)
    {
        $this->cObj = $cObj;
        $this->languageMenuProcessor = $languageMenuProcessor;
    }

    public function __invoke(ModifyHrefLangTagsEvent $event): void
    {
        $jobOfferAvailabilityChecker = GeneralUtility::makeInstance(JobOfferAvailability::class);
        if ($jobOfferAvailabilityChecker->getJobOfferIdFromRequest() > 0) {
            $allHrefLangs = $event->getHrefLangs();

            $languages = $this->languageMenuProcessor->process($this->cObj, [], [], []);
            $errorTriggered = false;
            foreach ($languages['languagemenu'] as $language) {
                $hreflangKey = $language['hreflang'];
                // skip all languages which are not used in hreflang
                if (!isset($allHrefLangs[$hreflangKey]) || $hreflangKey === 'x-default') {
                    continue;
                }

                try {
                    $check = $jobOfferAvailabilityChecker->check($language['languageId']);

                    if (!$check) {
                        unset($allHrefLangs[$hreflangKey]);
                    }
                } catch (\UnexpectedValueException) {
                    $errorTriggered = true;
                }
            }

            if (!$errorTriggered) {
                if (count($allHrefLangs) <= 2) {
                    unset($allHrefLangs['x-default']);
                }
                $event->setHrefLangs($allHrefLangs);
            }
        }
    }

    public function setContentObjectRenderer(ContentObjectRenderer $cObj): void
    {
        $this->cObj = $cObj;
    }
}
