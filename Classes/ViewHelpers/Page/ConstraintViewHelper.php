<?php

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2024 Bastian Holzem <b.holzem@teufels.com>, teufels GmbH
 *
 ***/

namespace Teufels\Tt3Career\ViewHelpers\Page;

use Teufels\Tt3Career\Domain\Model\Constraint;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class ConstraintViewHelper extends AbstractViewHelper
{
	use CompileWithRenderStatic;

	protected static array $reflectionCache = [];

	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('currentPage', 'string', 'page', true);
		$this->registerArgument('constraint', 'mixed', 'constraint', true);
	}

	/**
	 * @param array                     $arguments
	 * @param \Closure                  $renderChildrenClosure
	 * @param RenderingContextInterface $renderingContext *
	 *
	 * @return array which contains the page and constraints for the pagination template
	 */
	public static function renderStatic(array                     $arguments,
										\Closure                  $renderChildrenClosure,
										RenderingContextInterface $renderingContext): array
	{
		$page = (string)$arguments["currentPage"];
		/** @var Constraint $constraint */
		$constraintArgument = $arguments["constraint"];

		$returnArguments = [
				"currentPage" => $page,
		];

		if ($constraintArgument === null) {
			return $returnArguments;
		}

		$properties = self::getReflectedProperties($constraintArgument);

		foreach ($properties as $property) {
			$type = $property->getType()?->getName() ?? 'array';

			$identifier = "constraint][{$property->getName()}]";
			if ($type === 'array') {
				$identifier .= "[]";
			}

			$returnArguments[$identifier] = $property->getValue($constraintArgument);
		}

		return $returnArguments;
	}

	protected static function getReflectedProperties(object $constraint): array
	{
		$class = get_class($constraint);

		return self::$reflectionCache[$class] ?? (new \ReflectionClass(get_class($constraint)))->getProperties();
	}
}
