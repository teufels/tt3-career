<?php

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2024 Bastian Holzem <b.holzem@teufels.com>, teufels GmbH
 *
 ***/

namespace Teufels\Tt3Career\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Filter utility class
 */
class Filter implements SingletonInterface
{
    /**
     * Get available filter for a certain page
     *
     * @param int $pageUid
     * @return array
     */
    public function getAvailableFilter($pageUid): array
    {
        $filters = $this->getFilterFromTsConfig($pageUid);
        return $filters;
    }

    /**
     * Get filter defined in TsConfig
     *
     * @param $pageUid
     * @return array
     */
    protected function getFilterFromTsConfig(int $pageUid): array
    {
        $filter = [];
        $pagesTsConfig = BackendUtility::getPagesTSconfig($pageUid);
        if (isset($pagesTsConfig['tx_tt3career.']['settings.']['filter.']) && is_array($pagesTsConfig['tx_tt3career.']['settings.']['filter.'])) {
            $filter = $pagesTsConfig['tx_tt3career.']['settings.']['filter.'];
        }

        return $filter;
    }
}
