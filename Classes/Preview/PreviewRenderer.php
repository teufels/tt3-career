<?php

declare(strict_types=1);

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2024 Bastian Holzem <b.holzem@teufels.com>, teufels GmbH
 *
 ***/

namespace Teufels\Tt3Career\Preview;

use Doctrine\DBAL\Connection;
use phpDocumentor\Reflection\Types\Boolean;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Preview\StandardContentPreviewRenderer;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore;
use TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumnItem;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Type\Bitmask\Permission;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Render selected options of plugin in Web>Page module
 */
class PreviewRenderer extends StandardContentPreviewRenderer
{
    /**
     * Table information
     */
    public array $tableData = [];

    /**
     * Flexform information
     */
    public array $flexformData = [];

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    protected const LLPATH = 'LLL:EXT:tt3_career/Resources/Private/Language/locallang_be.xlf:';

    public function __construct()
    {
        $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
    }

    protected function renderContentElementPreviewFromFluidTemplate(array $row, ?GridColumnItem $item = null): ?string
    {
        $this->tableData = [];
        $flexforms = GeneralUtility::xml2array($row['pi_flexform']);
        if (is_string($flexforms)) {
            return 'ERROR: ' . htmlspecialchars($flexforms);
        }
        $this->flexformData = (array)$flexforms;

        //listing + watchlist + detail
        $this->getStartingPoint();
        $this->getSetting('showPid');
        $this->getSetting('listPid');
        $this->getSetting('backPid');
        $this->getSetting('limit', 'additional', false);
        $this->getSetting('hidePagination', 'additional', false, true);
        if(!isset($this->tableData['hidePagination'])) {
            $this->getSetting('list.paginate.itemsPerPage', 'additional', false);
        }
        $this->getOrderSettings();

        //filter
        $this->getFilterSetting();
        $this->getPreFilterSetting('careerLevel');
        $this->getPreFilterSetting('department');
        $this->getPreFilterSetting('location');
        $this->getPreFilterSetting('country');

        //template layout
        $this->getTemplateLayoutSetting();

        //watchlistButton
        $this->getSetting('watchlistPid');

        $row['tableData'] = $this->tableData;
        return parent::renderContentElementPreviewFromFluidTemplate($row);
    }

    protected function getRecordData(int $id, string $table = 'pages'): string
    {
        $record = BackendUtilityCore::getRecord($table, $id);

        if (is_array($record)) {
            $data = '<span data-toggle="tooltip" data-placement="top" data-title="id=' . $record['uid'] . '">'
                . $this->iconFactory->getIconForRecord($table, $record, Icon::SIZE_SMALL)->render()
                . '</span> ';
            $content = BackendUtilityCore::wrapClickMenuOnIcon(
                $data,
                $table,
                $record['uid'],
                true,
                $record
            );

            $linkTitle = htmlspecialchars(BackendUtilityCore::getRecordTitle($table, $record));

            if ($table === 'pages') {
                $id = $record['uid'];

                $request = $this->getRequest();
                $currentPageId = (int)($request->getParsedBody()['id'] ?? $request->getQueryParams()['id'] ?? 0);

                $link = htmlspecialchars($this->getEditLink($record, $currentPageId));
                $switchLabel = $this->getLanguageService()->sL(self::LLPATH . 'pagemodule.switchToPage');
                $content .= ' <a href="#" data-toggle="tooltip" data-placement="top" data-title="' . $switchLabel . '" onclick=\'top.jump("' . $link . '", "web_layout", "web", ' . $id . ');return false\'>' . $linkTitle . '</a>';
            } else {
                $content .= $linkTitle;
            }
        } else {
            $text = sprintf(
                $this->getLanguageService()->sL(self::LLPATH . 'pagemodule.recordNotAvailable'),
                $id
            );
            $content = $this->generateCallout($text);
        }

        return $content;
    }

    protected function getEditLink(array $row, int $currentPageUid): string
    {
        $editLink = '';
        $localCalcPerms = $GLOBALS['BE_USER']->calcPerms(BackendUtilityCore::getRecord('pages', $row['uid']));
        $permsEdit = $localCalcPerms & Permission::PAGE_EDIT;
        if ($permsEdit) {
            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
            $returnUrl = $uriBuilder->buildUriFromRoute('web_layout', ['id' => $currentPageUid]);
            $editLink = $uriBuilder->buildUriFromRoute('web_layout', [
                'id' => $row['uid'],
                'returnUrl' => $returnUrl,
            ]);
        }
        return (string)$editLink;
    }

    protected function getFieldFromFlexform(string $key, string $sheet = 'sDEF'): ?string
    {
        $flexform = $this->flexformData;

        if (isset($flexform['data'])) {
            $flexform = $flexform['data'];
            if (isset($flexform[$sheet]['lDEF'][$key]['vDEF'])
            ) {
                return $flexform[$sheet]['lDEF'][$key]['vDEF'];
            }
        }

        return null;
    }

    protected function getSetting(string $key, string $sheet = 'sDEF', bool $isRecord = true, bool $isBool = false): void
    {
        $content = $this->getFieldFromFlexform('settings.' . $key, $sheet);

        if($isRecord && ((int)$content > 0)) {
            $content = $this->getRecordData((int)$content);
        } else {
            $content = $this->getFieldFromFlexform('settings.' . $key, $sheet);
        }

        if ($content) {
            if($isBool) {
                $content = '&#10003;';
            }
            $this->tableData[$key] = [
                $this->getLanguageService()->sL(self::LLPATH . 'flexforms_' . $sheet . '.' . $key),
                $content,
            ];
        }
    }

    protected function getStartingPoint(): void
    {
        $value = $this->getFieldFromFlexform('persistence.storagePid');

        if (!empty($value)) {
            $pageIds = GeneralUtility::intExplode(',', $value, true);
            $pagesOut = [];

            foreach ($pageIds as $id) {
                $pagesOut[] = $this->getRecordData($id, 'pages');
            }

            $recursiveLevel = (int)$this->getFieldFromFlexform('persistence.recursive');
            $recursiveLevelText = '';
            if ($recursiveLevel === 250) {
                $recursiveLevelText = $this->getLanguageService()->sL('LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:recursive.I.5');
            } elseif ($recursiveLevel > 0) {
                $recursiveLevelText = $this->getLanguageService()->sL('LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:recursive.I.' . $recursiveLevel);
            }

            if (!empty($recursiveLevelText)) {
                $recursiveLevelText = '<br />' .
                    htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.recursive')) . ' ' .
                    $recursiveLevelText;
            }

            $this->tableData['startingPoint'] = [
                $this->getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.startingpoint'),
                implode(', ', $pagesOut) . $recursiveLevelText,
            ];
        }
    }

    protected function getOrderSettings(): void
    {
        $orderField = $this->getFieldFromFlexform('settings.orderBy', 'order');

        if (!empty($orderField)) {
            $text = $this->getLanguageService()->sL(self::LLPATH . 'flexforms_order.orderBy.' . $orderField);

            // Order direction (asc, desc)
            $orderDirection = $this->getOrderDirectionSetting();
            if ($orderDirection) {
                $text .= ', ' . strtolower($orderDirection);
            }

            // Top job offers first [tbd.]
            /*
            $topJobOffers = $this->getTopJobOffersFirstSetting();
            if ($topJobOffers) {
                $text .= '<br />' . $topJobOffers;
            }
            */

            $this->tableData[] = [
                $this->getLanguageService()->sL(self::LLPATH . 'flexforms_order.orderBy'),
                $text,
            ];
        }
    }

    protected function getOrderDirectionSetting(): string
    {
        $text = '';

        $orderDirection = $this->getFieldFromFlexform('settings.orderDirection', 'order');
        if (!empty($orderDirection)) {
            $text = $this->getLanguageService()->sL(self::LLPATH . 'flexforms_order.orderDirection.' . $orderDirection);
        }

        return $text;
    }

    protected function getFilterSetting(): void {
        $values = $this->getFieldFromFlexform('settings.filter', 'filter');
        if (!empty($values)) {
            $this->tableData[] = [
                $this->getLanguageService()->sL(self::LLPATH . 'flexforms_filter.filter'),
                $this->renderValuesAsList($values)
            ];
        }
    }

    protected function getPreFilterSetting(string $key): void {
        $values = $this->getFieldFromFlexform('settings.prefilter.' . $key, 'filter');
        if (!empty($values)) {
            $this->tableData[] = [
                $this->getLanguageService()->sL(self::LLPATH . 'flexforms_filter.prefilter.' . $key),
                $this->renderValuesAsList($values)
            ];
        }
    }

    protected function getTemplateLayoutSetting():void {
        $templateLayout = $this->getFieldFromFlexform('settings.templateLayout', 'template');
        if (!empty($templateLayout)) {
            $this->tableData[] = [
                $this->getLanguageService()->sL(self::LLPATH . 'flexforms_template.templateLayout'),
                $templateLayout,
            ];
        }
    }

    private function renderValuesAsList(string $values): string {
        $valuesArray = explode(",", $values);
        $output = "<ul class='m-0 ps-3 py-0'><li>" . implode("</li><li>", array_map('htmlspecialchars', $valuesArray)) . "</li></ul>";
        return $output;
    }

    /**
     * Helper Fucntion to get ServerRequestInterface
     *
     * @return ServerRequestInterface
     */
    protected function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }
}
