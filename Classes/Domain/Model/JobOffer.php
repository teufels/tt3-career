<?php

declare(strict_types=1);

namespace Teufels\Tt3Career\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * JobOffer
 */
class JobOffer extends AbstractEntity
{

    /**
     * pathSegment
     *
     * @var string
     */
    protected $pathSegment = null;

    /**
     * backendTitle
     *
     * @var string
     */
    protected $backendTitle = '';

    /**
     * headerImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $headerImage = null;

    /**
     * headerText
     *
     * @var string
     */
    protected $headerText = '';

    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * startDate
     *
     * @var \DateTime
     */
    protected $startDate = null;

    /**
     * employmenttype
     *
     * @var string
     */
    protected $employmenttype = '';

    /**
     * careerLevel
     *
     * @var string
     */
    protected $careerLevel = '';

    /**
     * company
     *
     * @var string
     */
    protected $company = '';

    /**
     * department
     *
     * @var string
     */
    protected $department = '';

    /**
     * location
     *
     * @var string
     */
    protected $location = '';

    /**
     * additionalLocations
     *
     * @var string
     */
    protected $additionalLocations = "";

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * intro
     *
     * @var string
     */
    protected $intro = '';

    /**
     * task
     *
     * @var string
     */
    protected $task = '';

    /**
     * profile
     *
     * @var string
     */
    protected $profile = '';

    /**
     * benefits
     *
     * @var string
     */
    protected $benefits = '';

    /**
     * information
     *
     * @var string
     */
    protected $information = '';

    /**
     * download
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $download = null;

    /**
     * externalLink
     *
     * @var string
     */
    protected $externalLink = '';

    /**
     * metaTitle
     *
     * @var string
     */
    protected $metaTitle = '';

    /**
     * metaDescription
     *
     * @var string
     */
    protected $metaDescription = '';

    /**
     * metaKeywords
     *
     * @var string
     */
    protected $metaKeywords = '';

    /**
     * ogTitle
     *
     * @var string
     */
    protected $ogTitle = '';

    /**
     * ogDescription
     *
     * @var string
     */
    protected $ogDescription = '';

    /**
     * ogImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $ogImage = null;

    /**
     * twitterTitle
     *
     * @var string
     */
    protected $twitterTitle = '';

    /**
     * twitterDescription
     *
     * @var string
     */
    protected $twitterDescription = '';

    /**
     * twitterImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $twitterImage = null;

    /**
     * twitterCard
     *
     * @var string
     */
    protected $twitterCard = '';

    /**
     * googleforjobs
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Teufels\Tt3Career\Domain\Model\GoogleForJobs>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $googleforjobs = null;

    /**
     * contact
     *
     * @var \FriendsOfTYPO3\TtAddress\Domain\Model\Address
     */
    protected $contact = null;

    /**
     * additionalReceiver
     *
     * @var string
     */
    protected $additionalReceiver = '';


    /**
     * categories
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
     */
    protected $categories;

    /**
     * __construct
     */
    public function __construct()
    {

        $this->categories = new ObjectStorage();

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->googleforjobs = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the startDate
     *
     * @return \DateTime $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the startDate
     *
     * @param \DateTime $startDate
     * @return void
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Returns the employmenttype
     *
     * @return string $employmenttype
     */
    public function getEmploymenttype()
    {
        return $this->employmenttype;
    }

    /**
     * Sets the employmenttype
     *
     * @param string $employmenttype
     * @return void
     */
    public function setEmploymenttype($employmenttype)
    {
        $this->employmenttype = $employmenttype;
    }

    /**
     * @return string[]
     */
    public function getDeserializedEmploymenttypes()
    {
        return explode(',', $this->employmenttype);
    }

    /**
     * Returns the careerLevel
     *
     * @return string $careerLevel
     */
    public function getCareerLevel()
    {
        return $this->careerLevel;
    }

    /**
     * Sets the careerLevel
     *
     * @param string $careerLevel
     * @return void
     */
    public function setCareerLevel($careerLevel)
    {
        $this->careerLevele = $careerLevel;
    }

    /**
     * Returns the company
     *
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     *
     * @param string $company
     * @return void
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }
    
    /**
     * Returns the department
     *
     * @return string $departmentl
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Sets the department
     *
     * @param string $department
     * @return void
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * Returns the location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param string $location
     * @return void
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Returns the additionalLocations
     *
     * @return string $additionalLocations
     */
    public function getAdditionalLocations()
    {
        return $this->additionalLocations;
    }

    /**
     * Sets the additionalLocations
     *
     * @param string $additionalLocations
     * @return void
     */
    public function setAdditionalLocations($additionalLocations)
    {
        $this->additionalLocations = $additionalLocations;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the intro
     *
     * @return string $intro
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Sets the intro
     *
     * @param string $intro
     * @return void
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    /**
     * Returns the task
     *
     * @return string $task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Sets the task
     *
     * @param string $task
     * @return void
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * Returns the profile
     *
     * @return string $profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Sets the profile
     *
     * @param string $profile
     * @return void
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * Returns the benefits
     *
     * @return string $benefits
     */
    public function getBenefits()
    {
        return $this->benefits;
    }

    /**
     * Sets the benefits
     *
     * @param string $benefits
     * @return void
     */
    public function setBenefits($benefits)
    {
        $this->benefits = $benefits;
    }

    /**
     * Returns the information
     *
     * @return string $information
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * Sets the information
     *
     * @param string $information
     * @return void
     */
    public function setInformation($information)
    {
        $this->information = $information;
    }

    /**
     * Returns the download
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $download
     */
    public function getDownload()
    {
        return $this->download;
    }

    /**
     * Sets the download
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $download
     * @return void
     */
    public function setDownload(\TYPO3\CMS\Extbase\Domain\Model\FileReference $download)
    {
        $this->download = $download;
    }

    /**
     * Adds a GoogleForJobs
     *
     * @param \Teufels\Tt3Career\Domain\Model\GoogleForJobs $googleforjob
     * @return void
     */
    public function addGoogleforjob(\Teufels\Tt3Career\Domain\Model\GoogleForJobs $googleforjob)
    {
        $this->googleforjobs->attach($googleforjob);
    }

    /**
     * Removes a GoogleForJobs
     *
     * @param \Teufels\Tt3Career\Domain\Model\GoogleForJobs $googleforjobToRemove The GoogleForJobs to be removed
     * @return void
     */
    public function removeGoogleforjob(\Teufels\Tt3Career\Domain\Model\GoogleForJobs $googleforjobToRemove)
    {
        $this->googleforjobs->detach($googleforjobToRemove);
    }

    /**
     * Returns the googleforjobs
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Teufels\Tt3Career\Domain\Model\GoogleForJobs> $googleforjobs
     */
    public function getGoogleforjobs()
    {
        return $this->googleforjobs;
    }

    /**
     * Sets the googleforjobs
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Teufels\Tt3Career\Domain\Model\GoogleForJobs> $googleforjobs
     * @return void
     */
    public function setGoogleforjobs(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $googleforjobs)
    {
        $this->googleforjobs = $googleforjobs;
    }

    /**
     * Returns the externalLink
     *
     * @return string $externalLink
     */
    public function getExternalLink()
    {
        return $this->externalLink;
    }

    /**
     * Sets the externalLink
     *
     * @param string $externalLink
     * @return void
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;
    }

    /**
     * Returns the contact
     *
     * @return \FriendsOfTYPO3\TtAddress\Domain\Model\Address $contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Sets the contact
     *
     * @param \FriendsOfTYPO3\TtAddress\Domain\Model\Address $contact
     * @return void
     */
    public function setContact(\FriendsOfTYPO3\TtAddress\Domain\Model\Address $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get path segment
     *
     * @return string
     */
    public function getPathSegment()
    {
        return $this->pathSegment;
    }

    /**
     * Set path segment
     *
     * @param string $pathSegment
     */
    public function setPathSegment($pathSegment)
    {
        $this->pathSegment = $pathSegment;
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->googleforjobs = $this->googleforjobs ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the headerImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $headerImage
     */
    public function getHeaderImage()
    {
        return $this->headerImage;
    }

    /**
     * Sets the headerImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $headerImage
     * @return void
     */
    public function setHeaderImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $headerImage)
    {
        $this->headerImage = $headerImage;
    }

    /**
     * Returns the headerText
     *
     * @return string $headerText
     */
    public function getHeaderText()
    {
        return $this->headerText;
    }

    /**
     * Sets the headerText
     *
     * @param string $headerText
     * @return void
     */
    public function setHeaderText(string $headerText)
    {
        $this->headerText = $headerText;
    }

    /**
     * Returns the metaTitle
     *
     * @return string $metaTitle
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Sets the metaTitle
     *
     * @param string $metaTitle
     * @return void
     */
    public function setMetaTitle(string $metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * Returns the metaDescription
     *
     * @return string $metaDescription
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Sets the metaDescription
     *
     * @param string $metaDescription
     * @return void
     */
    public function setMetaDescription(string $metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * Returns the metaKeywords
     *
     * @return string $metaKeywords
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Sets the metaKeywords
     *
     * @param string $metaKeywords
     * @return void
     */
    public function setMetaKeywords(string $metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * Returns the ogTitle
     *
     * @return string $ogTitle
     */
    public function getOgTitle()
    {
        return $this->ogTitle;
    }

    /**
     * Sets the ogTitle
     *
     * @param string $ogTitle
     * @return void
     */
    public function setOgTitle(string $ogTitle)
    {
        $this->ogTitle = $ogTitle;
    }

    /**
     * Returns the ogDescription
     *
     * @return string $ogDescription
     */
    public function getOgDescription()
    {
        return $this->ogDescription;
    }

    /**
     * Sets the ogDescription
     *
     * @param string $ogDescription
     * @return void
     */
    public function setOgDescription(string $ogDescription)
    {
        $this->ogDescription = $ogDescription;
    }

    /**
     * Returns the ogImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $ogImage
     */
    public function getOgImage()
    {
        return $this->ogImage;
    }

    /**
     * Sets the ogImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $ogImage
     * @return void
     */
    public function setOgImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $ogImage)
    {
        $this->ogImage = $ogImage;
    }

    /**
     * Returns the twitterTitle
     *
     * @return string $twitterTitle
     */
    public function getTwitterTitle()
    {
        return $this->twitterTitle;
    }

    /**
     * Sets the twitterTitle
     *
     * @param string $twitterTitle
     * @return void
     */
    public function setTwitterTitle(string $twitterTitle)
    {
        $this->twitterTitle = $twitterTitle;
    }

    /**
     * Returns the twitterDescription
     *
     * @return string $twitterDescription
     */
    public function getTwitterDescription()
    {
        return $this->twitterDescription;
    }

    /**
     * Sets the twitterDescription
     *
     * @param string $twitterDescription
     * @return void
     */
    public function setTwitterDescription(string $twitterDescription)
    {
        $this->twitterDescription = $twitterDescription;
    }

    /**
     * Returns the twitterImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $twitterImage
     */
    public function getTwitterImage()
    {
        return $this->twitterImage;
    }

    /**
     * Sets the twitterImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $twitterImage
     * @return void
     */
    public function setTwitterImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $twitterImage)
    {
        $this->twitterImage = $twitterImage;
    }

    /**
     * Returns the twitterCard
     *
     * @return string twitterCard
     */
    public function getTwitterCard()
    {
        return $this->twitterCard;
    }

    /**
     * Sets the twitterCard
     *
     * @param string $twitterCard
     * @return void
     */
    public function setTwitterCard($twitterCard)
    {
        $this->twitterCard = $twitterCard;
    }

    /**
     * Returns the additionalReceiver
     *
     * @return string $additionalReceiver
     */
    public function getAdditionalReceiver()
    {
        return $this->additionalReceiver;
    }

    /**
     * Sets the additionalReceiver
     *
     * @param string $additionalReceiver
     * @return void
     */
    public function setAdditionalReceiver($additionalReceiver)
    {
        $this->additionalReceiver = $additionalReceiver;
    }

    /**
     * Returns the categories
     *
     * @return ObjectStorage
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * Sets the categories
     *
     * @param ObjectStorage $categories
     */
    public function setCategories(ObjectStorage $categories)
    {
        $this->categories = $categories;
    }
}
