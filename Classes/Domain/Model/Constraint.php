<?php

declare(strict_types=1);

namespace Teufels\Tt3Career\Domain\Model;

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2024 Bastian Holzem <b.holzem@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Class Constraint
 *
 * @package Teufels\Tt3Career\Domain\Model
 */
class Constraint
{
    /** @var array
     */
    protected array $company = [];

    /** @var array
     */
    protected array $department = [];

    /** @var array
     */
    protected array $careerLevel = [];

    /** @var array
     */
    protected array $employmentType = [];

    /** @var array
     */
    protected array $location = [];

    /** @var array
     */
    protected array $country = [];

    /** @var array
     */
    protected array $sword = [];

    /**
     * @return array
     */
    public function getCompany(): array
    {
        return $this->company;
    }

    /**
     * @param array $company
     */
    public function setCompany(array $company): void
    {
        $this->company = $company;
    }

    /**
     * @return array
     */
    public function getDepartment(): array
    {
        return $this->department;
    }

    /**
     * @param array $department
     */
    public function setDepartment(array $department): void
    {
        $this->department = $department;
    }

    /**
     * @return array
     */
    public function getCareerLevel(): array
    {
        return $this->careerLevel;
    }

    /**
     * @param array $careerLevel
     */
    public function setCareerLevel(array $careerLevel): void
    {
        $this->careerLevel = $careerLevel;
    }

    /**
     * @return array
     */
    public function getEmploymentType(): array
    {
        return $this->employmentType;
    }

    /**
     * @param array $employmentType
     */
    public function setEmploymentType(array $employmentType): void
    {
        $this->employmentType = $employmentType;
    }

    /**
     * @return array
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    /**
     * @param array $location
     */
    public function setLocation(array $location): void
    {
        $this->location = $location;
    }

    /**
     * @return array
     */
    public function getCountry(): array
    {
        return $this->country;
    }

    /**
     * @param array $country
     */
    public function setCountry(array $country): void
    {
        $this->country = $country;
    }

    /**
     * @return array
     */
    public function getSword(): array
    {
        return $this->sword;
    }

    /**
     * @param array $sword
     */
    public function setSword(array $sword): void
    {
        $this->sword = $sword;
    }
}