<?php

declare(strict_types=1);

namespace Teufels\Tt3Career\Domain\Repository;

use Brainworxx\Krexx\Krexx;
use Doctrine\DBAL\Exception;
use phpDocumentor\Reflection\Types\Boolean;
use Teufels\Tt3Career\Domain\Model\Constraint;
use Teufels\Tt3Career\Domain\Model\JobOffer;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Reflection\ReflectionService;

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for JobOffers
 */
class JobOfferRepository extends Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];

    /**
     * Helper function for building the sql for categories
     *
     * @param array                                       $categories Array of category uids
     * @param \TYPO3\CMS\Core\Database\Query\QueryBuilder $qb         Query Builder to add to
     *
     * @return \TYPO3\CMS\Core\Database\Query\QueryBuilder
     */
    /**
    private function buildCategoriesToSQL(array $categories, QueryBuilder $qb)
    {
        $statement = "";
        for ($i = 0, $iMax = count($categories); $i < $iMax; $i++)
        {
            if ($i == 0)
            {
                if (count($categories) > 1)
                {
                    $qb->andWhere($qb->expr()->eq("sys_category_record_mm.uid_local", $categories[$i]));

                    $statement .= "AND (sys_category_record_mm.uid_local = ".$categories[$i]." ";
                }
                else
                {
                    $qb->andWhere($qb->expr()->eq("sys_category_record_mm.uid_local", $categories[$i]));

                    $statement .= "AND sys_category_record_mm.uid_local = ".$categories[$i]." ";
                }
            }
            else
            {
                $qb->orWhere($qb->expr()->eq("sys_category_record_mm.uid_local", $categories[$i]));
                $statement .= "OR sys_category_record_mm.uid_local = ".$categories[$i]." ";
                if ($i == count($categories) - 1)
                {
                    $statement .= ")";
                }
            }
        }

        return $qb;
    }
    */

    /**
     * Helper function for removing duplicate entries from an array based on the uid.
     *
     * @param array $array
     * @return array
     */
    private function removeDuplicates(array $array): array
    {
        $unique = [];
        $uids = [];
        foreach ($array as $item) {
            if (!in_array($item->getUid(), $uids)) {
                $uids[] = $item->getUid();
                $unique[] = $item;
            }
        }
        return $unique;
    }

    /**
     * Helper function for queryBuilder
     *
     * @param string $table
     *
     * @return \TYPO3\CMS\Core\Database\Query\QueryBuilder
     */
    private function getQueryBuilder(string $table)
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
    }

    /**
     * Helper function to executes a query based on the given constraints and merges the results into the result array.
     *
     * @param array $constraints
     * @param int $limit
     * @param array $result
     * @return array
     */
    private function executeQueryToArray(array $constraints, int $limit, array $result): array
    {
        $query = $this->createQuery();

        $query->matching($query->logicalAnd(...$constraints));
        $query->setLimit($limit);
        $queryResults = $query->execute()->toArray();

        return array_merge($result, $queryResults);
    }

    /**
     * Executes a query to fetch random job offers.
     *
     * @param int $limit
     * @param array $result
     * @param array $excludeUids
     * @return array
     */
    private function fetchRandomJobs(int $limit, array $result, array $excludeUids): array
    {
        $query = $this->createQuery();

        if (!empty($excludeUids)) {
            $query->matching($query->logicalNot($query->in('uid', $excludeUids)));
        }

        $randomJobs = $query->execute()->toArray();
        shuffle($randomJobs);
        $randomJobs = array_slice($randomJobs, 0, $limit);

        return array_merge($result, $randomJobs);
    }

    /**
     * Gets all values for a given field
     *
     * @param string     $field
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllValuesForField(string $field, int $languageUid, array $storagePageIds = null, array $categories = null): array
    {
        $qb = $this->getQueryBuilder("tx_tt3career_domain_model_joboffer");

        $qb->select("$field AS value", "sys_language_uid")
            ->groupBy("value", "sys_language_uid")
            ->from("tx_tt3career_domain_model_joboffer")
            ->where($qb->expr()->in("sys_language_uid", [$languageUid, -1]));

        if (!empty($storagePageIds)) {
            $qb->andWhere($qb->expr()->in("pid", $storagePageIds));
        }

        if (isset($categories)) {
            $qb->join("tx_tt3career_domain_model_joboffer",
                "sys_category_record_mm",
                "sys_category_record_mm",
                $qb->expr()->eq("tx_tt3career_domain_model_joboffer.uid",
                    "sys_category_record_mm.uid_foreign"));
            $qb = $this->buildCategoriesToSQL($categories, $qb);
        }

        $qb->orderBy('value', QueryInterface::ORDER_ASCENDING);


        /** @var array $result */
        $result = $qb->executeQuery()->fetchAllAssociative();

        return array_column($result, 'value');
    }

    /**
     * Gets all Employment Types
     *
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllEmploymentTypes(int $languageUid, array $storagePageIds = null, array $categories = null): array
    {
        $result = $this->findAllValuesForField('employmenttype', $languageUid, $storagePageIds, $categories);
        $return = [];
        foreach ($result as $string) {
            $explodedString = explode(',', $string);
            if (count($explodedString) < 2) {
                $return[] = $string;
            } else {
                $return = array_merge($return, $explodedString);
            }
        }

        $result = array_unique($return);
        asort($result);

        return $result;
    }

    /**
     * Gets all Carrer Levels
     *
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllCareerLevels(int $languageUid, array $storagePageIds = null, array $categories = null): array
    {
        return $this->findAllValuesForField('career_level', $languageUid, $storagePageIds, $categories);
    }

    /**
     * Gets all Companies
     *
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllCompanies(int $languageUid, array $storagePageIds = null, array $categories = null): array
    {
        return $this->findAllValuesForField('company', $languageUid, $storagePageIds, $categories);
    }

    /**
     * Gets all Departments
     *
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllDepartments(int $languageUid, array $storagePageIds = null, array $categories = null): array
    {
        return $this->findAllValuesForField('department', $languageUid, $storagePageIds, $categories);
    }

    /**
     * Gets all Locations
     *
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllLocations(int $languageUid, array $storagePageIds = null, array $categories = null): array
    {
        return $this->findAllValuesForField('location', $languageUid, $storagePageIds, $categories);
    }

    /**
     * Gets all Countrys
     *
     * @param int        $languageUid
     * @param array|null $storagePageIds
     * @param array|null $categories
     *
     * @return array
     * @throws Exception
     */
    public function findAllCountrys(int $languageUid,  array $storagePageIds = null, array $categories = null): array
    {
        return $this->findAllValuesForField('country', $languageUid, $storagePageIds, $categories);
    }

    /**
     * find all for auto suggestion
     * respect SysLanguageUid
     *
     * @return array
     */
    public function findAllForAutoSuggestion()
    {
        $languageAspect = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getAspect('language');
        $sys_language_uid = $languageAspect->getId();

        $queryBuilder = $this->getQueryBuilder("tx_tt3career_domain_model_joboffer");
        $queryBuilder
            ->select('title', 'subtitle', 'meta_keywords')
            ->from('tx_tt3career_domain_model_joboffer');

        if (isset($sys_language_uid)) {
            $queryBuilder->where(
                $queryBuilder->expr()->in("sys_language_uid", [$sys_language_uid, -1])
            );
        }

        $result = $queryBuilder->executeQuery()->fetchAllAssociative();
        return $result;
    }

    /**
     * @param array           $categories
     * @param array           $filterConfig
     * @param Constraint|null $constraint
     * @param array           $order
     * @param int             $limit
     *
     * @return array|QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Reflection\Exception\UnknownClassException
     */
    public function findByFilter(array      $categories = null,
                                 array      $filterConfig = [],
                                 Constraint $constraint = null,
                                            $order = null,
                                            $limit = null)
    {
        $query = $this->createQuery();
        $andConstraints = [];

        if ($constraint instanceof Constraint) {
            /** @var ReflectionService $reflection */
            $reflection = GeneralUtility::makeInstance(ReflectionService::class);
            $schema = $reflection->getClassSchema($constraint);
            $constraints = $schema->getProperties();

            foreach ($constraints as $index => $property) {
                // TYPO3 v9 compatibility
                $propertyName = is_array($property) ? $index : $property->getName();
                $propertyMethodName = GeneralUtility::underscoredToUpperCamelCase($propertyName);
                $array = $constraint->{'get' . $propertyMethodName}();

                if (empty($array)) {
                    continue;
                }

                $orConstraints = [];
                foreach ($array as $input) {
                    // Skip empty values. The prepend option, associated with 'All', returns this empty string.
                    if ($input === '') {
                        continue;
                    }

                    if (!isset($filterConfig[$propertyName])) {
                        throw new \RuntimeException("Missing TypoScript filter config for property: " . $propertyName);
                    }

                    $searchFields = explode(',', $filterConfig[$propertyName]['relation']);
                    foreach ($searchFields as $field) {
                        $orConstraints[] = match ($filterConfig[$propertyName]['relationType']) {
                            'equals' => $query->equals($field, $input),
                            'contains' => $query->contains($field, $input),
                            'in' => $query->in($field, $input),
                            'like' => $query->like($field, '%' . $input . '%'),
                            default => throw new \InvalidArgumentException(sprintf('Invalid relation type %s', $filterConfig[$propertyName]['relationType'])),
                        };
                    }
                }

                if (count($orConstraints) > 0) {
                    $andConstraints[] = $query->logicalOr(...$orConstraints);
                }
            }
        }

        if (!empty($categories)) {
            $orConstraints = [];
            foreach ($categories as $category) {
                $orConstraints[] = $query->contains('categories', $category);
            }
            $andConstraints[] = $query->logicalOr(...$orConstraints);
        }

        if (!empty($andConstraints)) {
            $query->matching($query->logicalAnd(...$andConstraints));
        }

        //order
        if(!empty($order)){
            $query->setOrderings($order);
        } else {
            $query->setOrderings($this->defaultOrderings);
        }

        //limit
        if(!empty($limit)){
            $query->setLimit((int)$limit);
        }

        return $query->execute();
    }


    /**
     * find all on watclist
     * ignore Storage
     * ignore SysLanguage
     *
     * @param array           $order
     * @param int             $limit
     *
     * @return array|QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Reflection\Exception\UnknownClassException
     */
    public function findAllOnWatchlist(array $order = null, $limit = null)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        //get all job offers on watchlsit as array
        $cookie_name = "tt3career_joboffer_watchlist";
        $watchlist = [];
        if (isset($_COOKIE[$cookie_name])) {
            $watchlist = array_map('intval',json_decode($_COOKIE[$cookie_name], true));
        }

        if (!empty($watchlist)) {
            $constraints = $query->in('uid', $watchlist);
            $query->matching($constraints);
        } else {
            $constraints = $query->in('uid', [-1]);
            $query->matching($constraints);
        }

        //order
        if(!empty($order)){
            $query->setOrderings($order);
        } else {
            $query->setOrderings($this->defaultOrderings);
        }

        //limit
        if(!empty($limit)){
            $query->setLimit((int)$limit);
        }

        return $query->execute();
    }

    /**
     * check if jobOffer uid exist in watchlist
     *
     * @param int $jobOfferUid
     * @return bool
     */
    public function isInWatchlist(int $jobOfferUid): bool
    {
        $cookie_name = "tt3career_joboffer_watchlist";
        if (isset($_COOKIE[$cookie_name])) {
            $watchlist = json_decode($_COOKIE[$cookie_name], true);

            if (in_array((string)$jobOfferUid, $watchlist)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer
     * @param int $limit
     * @return array
     */
    public function findRelatedJobs(\Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer, int $limit = 0): array
    {
        // If limit is 0, return an empty array
        if ($limit === 0) {
            return [];
        }

        $result = [];
        $keywordConstraints = [];
        $remainingLimit = $limit;

        // Exclude the original job offer
        $excludeOriginalJobOffer = $this->createQuery()->logicalNot($this->createQuery()->equals('uid', $jobOffer->getUid()));

        // Prepare keyword constraints
        if ($jobOffer->getMetaKeywords()) {
            $keywords = $jobOffer->getMetaKeywords();
            $keywordsArray = array_map('trim', explode(',', $keywords));
            foreach ($keywordsArray as $keyword) {
                $keywordConstraints[] = $this->createQuery()->like('meta_keywords', '%' . $keyword . '%');
            }
        }

        // Step 1: meta_keywords & career_level
        if (!empty($keywordConstraints)) {
            $constraints = [$excludeOriginalJobOffer, $this->createQuery()->logicalOr(...$keywordConstraints)];
            if ($jobOffer->getCareerLevel()) {
                $constraints[] = $this->createQuery()->equals('career_level', $jobOffer->getCareerLevel());
            }

            $result = $this->executeQueryToArray($constraints, $remainingLimit, $result);
            $result = $this->removeDuplicates($result);
            $remainingLimit = $limit - count($result);
        }

        // Step 2: department & location & career_level
        if ($remainingLimit > 0 && $jobOffer->getDepartment() && $jobOffer->getLocation()) {
            $constraints = [
                $excludeOriginalJobOffer,
                $this->createQuery()->equals('department', $jobOffer->getDepartment()),
                $this->createQuery()->equals('location', $jobOffer->getLocation())
            ];
            if ($jobOffer->getCareerLevel()) {
                $constraints[] = $this->createQuery()->equals('career_level', $jobOffer->getCareerLevel());
            }

            $result = $this->executeQueryToArray($constraints, $remainingLimit, $result);
            $result = $this->removeDuplicates($result);
            $remainingLimit = $limit - count($result);
        }

        // Step 3: meta_keywords
        if ($remainingLimit > 0 && !empty($keywordConstraints)) {
            $constraints = [$excludeOriginalJobOffer, $this->createQuery()->logicalOr(...$keywordConstraints)];

            $result = $this->executeQueryToArray($constraints, $remainingLimit, $result);
            $result = $this->removeDuplicates($result);
            $remainingLimit = $limit - count($result);
        }

        // Step 4: department & career_level
        if ($remainingLimit > 0 && $jobOffer->getDepartment()) {
            $constraints = [
                $excludeOriginalJobOffer,
                $this->createQuery()->equals('department', $jobOffer->getDepartment())
            ];
            if ($jobOffer->getCareerLevel()) {
                $constraints[] = $this->createQuery()->equals('career_level', $jobOffer->getCareerLevel());
            }

            $result = $this->executeQueryToArray($constraints, $remainingLimit, $result);
            $result = $this->removeDuplicates($result);
            $remainingLimit = $limit - count($result);
        }

        // Step 5: location & career_level
        if ($remainingLimit > 0 && $jobOffer->getLocation()) {
            $constraints = [
                $excludeOriginalJobOffer,
                $this->createQuery()->equals('location', $jobOffer->getLocation())
            ];
            if ($jobOffer->getCareerLevel()) {
                $constraints[] = $this->createQuery()->equals('career_level', $jobOffer->getCareerLevel());
            }

            $result = $this->executeQueryToArray($constraints, $remainingLimit, $result);
            $result = $this->removeDuplicates($result);
            $remainingLimit = $limit - count($result);
        }

        // Step 6: department & location
        if ($remainingLimit > 0 && $jobOffer->getDepartment() && $jobOffer->getLocation()) {
            $constraints = [
                $excludeOriginalJobOffer,
                $this->createQuery()->equals('department', $jobOffer->getDepartment()),
                $this->createQuery()->equals('location', $jobOffer->getLocation())
            ];

            $result = $this->executeQueryToArray($constraints, $remainingLimit, $result);
            $result = $this->removeDuplicates($result);
            $remainingLimit = $limit - count($result);
        }

        // Final Step: Fetch random jobs if limit not reached
        if ($remainingLimit > 0) {
            $excludeUids = array_map(function($item) {
                return $item->getUid();
            }, $result);
            $result = $this->fetchRandomJobs($remainingLimit, $result, $excludeUids);
        }

        return array_slice($result, 0, $limit);
    }


}
