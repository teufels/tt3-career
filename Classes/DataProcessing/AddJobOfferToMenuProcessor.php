<?php

declare(strict_types=1);

namespace Teufels\Tt3Career\DataProcessing;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Add the current jobOffer record to any menu, e.g. breadcrumb
 *
 * 20 = Teufels\Tt3Career\DataProcessing\AddJobOfferToMenuProcessor
 * 20.menus = breadcrumbMenu,specialMenu
 */
class AddJobOfferToMenuProcessor implements DataProcessorInterface
{

    /**
     * @param ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {
        if (!$processorConfiguration['menus']) {
            return $processedData;
        }
        $jobOfferRecord = $this->getJobOfferRecord();
        if ($jobOfferRecord) {
            $menus = GeneralUtility::trimExplode(',', $processorConfiguration['menus'], true);
            foreach ($menus as $menu) {
                if (isset($processedData[$menu])) {
                    $this->addJobOfferRecordToMenu($jobOfferRecord, $processedData[$menu]);
                }
            }
        }
        return $processedData;
    }

    /**
     * Add the jobOffer record to the menu items
     *
     * @param array $jobOfferRecord
     * @param array $menu
     *
     * @return void
     */
    protected function addJobOfferRecordToMenu(array $jobOfferRecord, array &$menu): void
    {
        foreach ($menu as &$menuItem) {
            $menuItem['current'] = 0;
        }

        $menu[] = [
            'data' => $jobOfferRecord,
            'title' => $jobOfferRecord['title'],
            'active' => 1,
            'current' => 1,
            'link' => GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'),
            'isJobOffer' => true
        ];
    }

    /**
     * Get the jobOffer record including possible translations
     *
     * @return array
     */
    protected function getJobOfferRecord(): array
    {
        $jobOfferId = 0;
        $vars = $request->getQueryParams()['tx_tt3career_careershow'] ?? null;
        if (isset($vars['jobOffer'])) {
            $jobOfferId = (int)$vars['jobOffer'];
        }

        if ($jobOfferId) {
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_tt3career_domain_model_joboffer');
            $row = $queryBuilder
                ->select('*')
                ->from('tx_tt3career_domain_model_joboffer')
                ->where(
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobOfferId, \PDO::PARAM_INT))
                )
                ->execute()
                ->fetch();

            if ($row) {
                // @extensionScannerIgnoreLine
                $row = $GLOBALS['TSFE']->sys_page->getRecordOverlay('tx_tt3career_domain_model_joboffer', $row, $this->getCurrentLanguage());
            }

            if (is_array($row) && !empty($row)) {
                return $row;
            }
        }
        return [];
    }

    /**
     * Get current language
     *
     * @return int
     */
    protected function getCurrentLanguage(): int
    {
        $languageId = 0;
        $context = GeneralUtility::makeInstance(Context::class);
        try {
            $languageId = $context->getPropertyFromAspect('language', 'contentId');
        } catch (AspectNotFoundException $e) {
            // do nothing
        }

        return (int)$languageId;
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTsfe(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }
}
