<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Career\DataProcessing;

use Teufels\Tt3Career\Seo\JobOfferAvailability;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Disable language item on a detail page if the jobOffer is not translated
 *
 * 42 = disable-language-menu-for-joboffer
 * 42.menus = languageMenu
 */
class DisableLanguageMenuProcessor implements DataProcessorInterface
{
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {
        if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.'])) {
            return $processedData;
        }

        if (!$processorConfiguration['menus']) {
            return $processedData;
        }
        $jobOfferId = $this->getJobOfferId();
        if ($jobOfferId === 0) {
            return $processedData;
        }
        $menus = GeneralUtility::trimExplode(',', $processorConfiguration['menus'], true);
        foreach ($menus as $menu) {
            if (isset($processedData[$menu])) {
                $this->handleMenu($jobOfferId, $processedData[$menu]);
            }
        }
        return $processedData;
    }

    protected function handleMenu(int $jobOfferId, array &$menu): void
    {
        $jobOfferAvailability = GeneralUtility::makeInstance(JobOfferAvailability::class);
        foreach ($menu as &$item) {
            if (!$item['available']) {
                continue;
            }
            try {
                $availability = $jobOfferAvailability->check((int)$item['languageId'], $jobOfferId);
                if (!$availability) {
                    $item['available'] = false;
                    $item['availableReason'] = 'jobOffer';
                }
            } catch (\Exception) {
            }
        }
    }

    protected function getJobOfferId(): int
    {
        $jobOfferId = 0;
        /** @var PageArguments $pageArguments */
        $pageArguments = $this->getRequest()->getAttribute('routing');
        if (isset($pageArguments->getRouteArguments()['tx_tt3career_careershow']['jobOffer'])) {
            $jobOfferId = (int)$pageArguments->getRouteArguments()['tx_tt3career_careershow']['jobOffer'];
        } elseif (isset($this->getRequest()->getQueryParams()['tx_tt3career_careershow']['jobOffer'])) {
            $jobOfferId = (int)$this->getRequest()->getQueryParams()['tx_tt3career_careershow']['jobOffer'];
        }

        return $jobOfferId;
    }

    protected function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }
}
