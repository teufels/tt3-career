<?php

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2024 Bastian Holzem <b.holzem@teufels.com>, teufels GmbH
 *
 ***/

namespace Teufels\Tt3Career\Hooks;

use FriendsOfTYPO3\TtAddress\Database\QueryGenerator;
use Teufels\Tt3Career\Domain\Repository\JobOfferRepository;
use Teufels\Tt3Career\Utility\Filter;
use Teufels\Tt3Career\Utility\TemplateLayout;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Userfunc to
 * add custom tempaltes
 */
class ItemsProcFunc
{
    /** @var TemplateLayout $templateLayoutsUtility */
    protected $templateLayoutsUtility;

    /** @var Filter $filter */
    protected $filter;

    /** @var JobOfferRepository $jobOfferRepository  */
    protected $jobOfferRepository = null;

    /** Flexform information */
    public array $flexformData = [];

    /**
     * ItemsProcFunc constructor.
     * @param TemplateLayout $templateLayout
     * @param Filter $filter
     */
    public function __construct(
        TemplateLayout $templateLayout,
        Filter $filter,
    ) {
        $this->templateLayoutsUtility = $templateLayout;
        $this->filter = $filter;
    }

    /**
     * @param JobOfferRepository $jobOfferRepository
     */
    public function injectJobOfferRepository(JobOfferRepository $jobOfferRepository)
    {
        $this->jobOfferRepository = $jobOfferRepository;
    }

    /**
     * Itemsproc function to extend the selection of filter in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_filter(array &$config): void
    {
        $pageId = 0;

        $currentColPos = $config['flexParentDatabaseRow']['colPos'] ?? null;
        if ($currentColPos === null) {
            return;
        }
        $pageId = $this->getPageId($config['flexParentDatabaseRow']['pid']);

        if ($pageId > 0) {
            $filters = $this->filter->getAvailableFilter($pageId);

            foreach ($filters as $key => $filter) {
                $additionalFilter = [
                    htmlspecialchars($this->getLanguageService()->sL($filter['label'])),
                    rtrim($key, '.')
                ];
                array_push($config['items'], $additionalFilter);
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of prefilter careerLevel in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_prefilter_careerLevel(array &$config): void
    {

        $this->flexformData = (array)$config['flexParentDatabaseRow']['pi_flexform'];
        $storagePageIds = $this->getStoragePidList();

        /** @var LanguageAspect $languageAspect */
        $languageAspect = GeneralUtility::makeInstance(Context::class)?->getAspect('language');
        $languageId = $languageAspect->getId();

        $availableItems = $this->jobOfferRepository->findAllCareerLevels($languageId, $storagePageIds);

        $selectedItems = [];
        if(!empty($config['row']['settings.prefilter.careerLevel'])) {
            $selectedItemString = $config['row']['settings.prefilter.careerLevel'];
            $selectedItems = array_map('trim', explode(',', $selectedItemString));
        }
        $allItems = array_unique(array_merge($availableItems, $selectedItems));

        if (!empty($allItems)) {
            foreach ($allItems as $item) {
                if($item){
                    $items = [
                        htmlspecialchars($this->getLanguageService()->sL($item)),
                        $item
                    ];
                    array_push($config['items'], $items);
                }
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of prefilter company in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_prefilter_company(array &$config): void
    {

        $this->flexformData = (array)$config['flexParentDatabaseRow']['pi_flexform'];
        $storagePageIds = $this->getStoragePidList();

        /** @var LanguageAspect $languageAspect */
        $languageAspect = GeneralUtility::makeInstance(Context::class)?->getAspect('language');
        $languageId = $languageAspect->getId();

        $availableItems = $this->jobOfferRepository->findAllCompanies($languageId, $storagePageIds);

        $selectedItems = [];
        if(!empty($config['row']['settings.prefilter.company'])) {
            $selectedItemString = $config['row']['settings.prefilter.company'];
            $selectedItems = array_map('trim', explode(',', $selectedItemString));
        }
        $allItems = array_unique(array_merge($availableItems, $selectedItems));

        if (!empty($allItems)) {
            foreach ($allItems as $item) {
                if($item){
                    $items = [
                        htmlspecialchars($this->getLanguageService()->sL($item)),
                        $item
                    ];
                    array_push($config['items'], $items);
                }
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of prefilter department in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_prefilter_department(array &$config): void
    {

        $this->flexformData = (array)$config['flexParentDatabaseRow']['pi_flexform'];
        $storagePageIds = $this->getStoragePidList();

        /** @var LanguageAspect $languageAspect */
        $languageAspect = GeneralUtility::makeInstance(Context::class)?->getAspect('language');
        $languageId = $languageAspect->getId();

        $availableItems = $this->jobOfferRepository->findAllDepartments($languageId, $storagePageIds);

        $selectedItems = [];
        if(!empty($config['row']['settings.prefilter.department'])) {
            $selectedItemString = $config['row']['settings.prefilter.department'];
            $selectedItems = array_map('trim', explode(',', $selectedItemString));
        }
        $allItems = array_unique(array_merge($availableItems, $selectedItems));

        if (!empty($allItems)) {
            foreach ($allItems as $item) {
                if($item){
                    $items = [
                        htmlspecialchars($this->getLanguageService()->sL($item)),
                        $item
                    ];
                    array_push($config['items'], $items);
                }
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of prefilter location in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_prefilter_location(array &$config): void
    {

        $this->flexformData = (array)$config['flexParentDatabaseRow']['pi_flexform'];
        $storagePageIds = $this->getStoragePidList();

        /** @var LanguageAspect $languageAspect */
        $languageAspect = GeneralUtility::makeInstance(Context::class)?->getAspect('language');
        $languageId = $languageAspect->getId();

        $availableItems = $this->jobOfferRepository->findAllLocations($languageId, $storagePageIds);

        $selectedItems = [];
        if(!empty($config['row']['settings.prefilter.location'])) {
            $selectedItemString = $config['row']['settings.prefilter.location'];
            $selectedItems = array_map('trim', explode(',', $selectedItemString));
        }
        $allItems = array_unique(array_merge($availableItems, $selectedItems));

        if (!empty($allItems)) {
            foreach ($allItems as $item) {
                if($item){
                    $items = [
                        htmlspecialchars($this->getLanguageService()->sL($item)),
                        $item
                    ];
                    array_push($config['items'], $items);
                }
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of prefilter country in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_prefilter_country(array &$config): void
    {

        $this->flexformData = (array)$config['flexParentDatabaseRow']['pi_flexform'];
        $storagePageIds = $this->getStoragePidList();

        /** @var LanguageAspect $languageAspect */
        $languageAspect = GeneralUtility::makeInstance(Context::class)?->getAspect('language');
        $languageId = $languageAspect->getId();

        $availableItems = $this->jobOfferRepository->findAllCountrys($languageId, $storagePageIds);

        $selectedItems = [];
        if(!empty($config['row']['settings.prefilter.country'])) {
            $selectedItemString = $config['row']['settings.prefilter.country'];
            $selectedItems = array_map('trim', explode(',', $selectedItemString));
        }
        $allItems = array_unique(array_merge($availableItems, $selectedItems));

        if (!empty($allItems)) {
            foreach ($allItems as $item) {
                if($item){
                    $items = [
                        htmlspecialchars($this->getLanguageService()->sL($item)),
                        $item
                    ];
                    array_push($config['items'], $items);
                }
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts (for list) in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_templateLayout_list(array &$config): void
    {
        $pageId = 0;

        $currentColPos = $config['flexParentDatabaseRow']['colPos'] ?? null;
        if ($currentColPos === null) {
            return;
        }
        $pageId = $this->getPageId($config['flexParentDatabaseRow']['pid']);

        if ($pageId > 0) {
            $templateLayouts = $this->templateLayoutsUtility->getAvailableTemplateLayouts($pageId);

            $templateLayouts = $this->reduceTemplateLayouts($templateLayouts, $currentColPos);
            foreach ($templateLayouts as $layout) {
                $additionalLayout = [
                    htmlspecialchars($this->getLanguageService()->sL($layout[0])),
                    $layout[1],
                ];
                array_push($config['items'], $additionalLayout);
            }
        }
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts (for detail) in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_templateLayout_detail(array &$config): void
    {
        $pageId = 0;

        $currentColPos = $config['flexParentDatabaseRow']['colPos'] ?? null;
        if ($currentColPos === null) {
            return;
        }
        $pageId = $this->getPageId($config['flexParentDatabaseRow']['pid']);

        if ($pageId > 0) {
            $templateLayouts = $this->templateLayoutsUtility->getAvailableTemplateLayouts($pageId,'detail');

            $templateLayouts = $this->reduceTemplateLayouts($templateLayouts, $currentColPos);
            foreach ($templateLayouts as $layout) {
                $additionalLayout = [
                    htmlspecialchars($this->getLanguageService()->sL($layout[0])),
                    $layout[1],
                ];
                array_push($config['items'], $additionalLayout);
            }
        }
    }

    /**
     * Reduce the template layouts by the ones that are not allowed in given colPos
     *
     * @param array $templateLayouts
     * @param int $currentColPos
     * @return array
     */
    protected function reduceTemplateLayouts($templateLayouts, $currentColPos): array
    {
        $currentColPos = (int)$currentColPos;
        $restrictions = [];
        $allLayouts = [];
        foreach ($templateLayouts as $key => $layout) {
            if (is_array($layout[0])) {
                if (isset($layout[0]['allowedColPos']) && str_ends_with((string)$layout[1], '.')) {
                    $layoutKey = substr($layout[1], 0, -1);
                    $restrictions[$layoutKey] = GeneralUtility::intExplode(',', $layout[0]['allowedColPos'], true);
                }
            } else {
                $allLayouts[$key] = $layout;
            }
        }
        if (!empty($restrictions)) {
            foreach ($restrictions as $restrictedIdentifier => $restrictedColPosList) {
                if (!in_array($currentColPos, $restrictedColPosList, true)) {
                    unset($allLayouts[$restrictedIdentifier]);
                }
            }
        }

        return $allLayouts;
    }

    /**
     * Get page id, if negative, then it is a "after record"
     *
     * @param int $pid
     * @return int
     */
    protected function getPageId($pid): int
    {
        $pid = (int)$pid;

        if ($pid > 0) {
            return $pid;
        }

        $row = BackendUtilityCore::getRecord('tt_content', abs($pid), 'uid,pid');
        return $row['pid'];
    }

    /**
     * Returns LanguageService
     *
     * @return \TYPO3\CMS\Core\Localization\LanguageService
     */
    protected function getLanguageService(): \TYPO3\CMS\Core\Localization\LanguageService
    {
        return $GLOBALS['LANG'];
    }

    /**
     * Get field from flexform
     *
     * @param string $key
     * @param string $sheet
     * @param string $field
     * @return mixed
     */
    protected function getFieldFromFlexform(string $key, string $sheet = 'sDEF', string $field = null): mixed
    {
        $flexform = $this->flexformData;

        if (isset($flexform['data'])) {
            $flexform = $flexform['data'];
            if (isset($flexform[$sheet]['lDEF'][$key]['vDEF'])
            ) {
                if(!empty($field)) {
                    $values = [];
                    foreach ($flexform[$sheet]['lDEF'][$key]['vDEF'] as $entry) {
                        $values[] = $entry[$field];
                    }
                    return $values;
                } else {
                    $values = [];
                    foreach ($flexform[$sheet]['lDEF'][$key]['vDEF'] as $entry) {
                        $values[] = $entry;
                    }
                    return $values;
                }
            }
        }

        return null;
    }

    /**
     * Retrieves subpages of given pageIds recursively until reached persistence.recursive
     *
     * @return null|array
     */
    protected function getStoragePidList(): mixed
    {
        $pageIds = (array)$this->getFieldFromFlexform('persistence.storagePid','sDEF','uid');

        if(isset($this->getFieldFromFlexform('persistence.recursive','sDEF')[0])) {
            $depth = $this->getFieldFromFlexform('persistence.recursive','sDEF')[0];
        } else {
            $depth = 0;
        }

        $pageReository = GeneralUtility::makeInstance(PageRepository::class);
        $pidList = $pageReository->getPageIdsRecursive($pageIds, (int)($depth ?? 0));

        return $pidList;
    }

}