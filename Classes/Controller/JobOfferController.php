<?php
declare(strict_types=1);

namespace Teufels\Tt3Career\Controller;

use Doctrine\DBAL\Exception;
use Psr\Http\Message\ResponseInterface;
use Teufels\Tt3Career\Domain\Model\Constraint;
use Teufels\Tt3Career\Domain\Model\JobOffer;
use Teufels\Tt3Career\Domain\Repository\JobOfferRepository;
use Teufels\Tt3Career\PageTitle\TitleProvider;
use Teufels\Tt3Career\Utility\Filter;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Http\ResponseFactory;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Pagination\SlidingWindowPagination;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3Fluid\Fluid\View\ViewInterface;

/***
 *
 * This file is part of the "tt3_career" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2022 teufels GmbH <digital@teufels.com>
 *
 ***/

/**
 * JobOfferController
 */
class JobOfferController extends ActionController
{

    /**
     * Filter Utility
     *
     * @var Filter $filter
     */
    protected $filter;

    /**
     * selected Filter
     *
     * @var array $selectedFilter
     */
    protected $selectedFilter = [];

    /**
     * seelected PreFilter
     *
     * @var array $selectedPreFilter
     */
    protected $selectedPreFilter = [];

    /**
     * all seelected Filter (Filter & Prefilter)
     *
     * @var array $allSelectedFilter
     */
    protected $allSelectedFilter = [];

    /**
     * jobOfferRepository
     *
     * @var JobOfferRepository
     */
    protected $jobOfferRepository = null;

    protected PageRenderer $pageRenderer;

    /**
     * constructor.
     * @param PageRenderer $pageRenderer
     * @param Filter $filter
     */
    public function __construct(
        PageRenderer $pageRenderer,
        Filter $filter
    ) {
        $this->pageRenderer = $pageRenderer;
        $this->filter = $filter;
    }

    /**
     * @param JobOfferRepository $jobOfferRepository
     */
    public function injectJobOfferRepository(JobOfferRepository $jobOfferRepository)
    {
        $this->jobOfferRepository = $jobOfferRepository;
    }

    /**
     * Initializes the view before invoking an action method.
     *
     * Override this method to solve assign variables common for all actions
     * or prepare the view in another way before the action is called.
     *
     * @param ViewInterface $view The view to be initialized
     */
    public function initializeView(ViewInterface $view): void
    {
        if (is_object($GLOBALS['TSFE']))
        {
            $view->assign('pageData', $GLOBALS['TSFE']->page);
        }

        //fallback templateLayout
        if(isset($this->settings['templateLayout'])){
            $this->settings['templateLayout'] = $this->settings['templateLayout'] ?: 'default';
        } else {
            $this->settings['templateLayout'] = 'default';
        }
    }
    
    /**
     * @return void
     * @throws InvalidQueryException
     * @throws NoSuchArgumentException
     */
    public function initializeListAction() {
        //set selected Filter arrays
        $this->setAllSelectedFilterArrays();

        $propertyMappingConfiguration = $this->arguments->getArgument("constraint")->getPropertyMappingConfiguration();
        foreach ($this->getCachedFilterOptions() as $index => $property) {
            $propertyMappingConfiguration->allowProperties($index);
        }
    }
    
    /**
     * Output a list view of job offers
     *
     * @param Constraint|null $constraint
     */
    public function listAction(Constraint $constraint = null): ResponseInterface
    {

        /**
         * Order
         */
        $order = $this->getOrder();

        /**
         * Limit
         */
        $limit =  (int)$this->settings['limit'] ?: 0;
        if(!empty($limit)){
            $this->settings['hidePagination'] = 1;
        }

        /**
         * Filter
         */
        // Get selected filter
        $filterConfig = $this->getSelectedFilterOptions();

        // Get cached filter options
        $filterOptions = $this->getCachedFilterOptions();

        /**
         * Constraint
         */
        if (trim(($this->settings['prefilter']['careerLevel'] ?? '')) !== '') {
            $constraint = $this->getPreFilterCareerLevel($constraint);
        }
        if (trim(($this->settings['prefilter']['company'] ?? '')) !== '') {
            $constraint = $this->getPreFilterCompany($constraint);
        }
        if (trim(($this->settings['prefilter']['department'] ?? '')) !== '') {
            $constraint = $this->getPreFilterDepartment($constraint);
        }
        if (trim(($this->settings['prefilter']['location'] ?? '')) !== '') {
            $constraint = $this->getPreFilterLocation($constraint);
        }
        if (trim(($this->settings['prefilter']['country'] ?? '')) !== '') {
            $constraint = $this->getPreFilterCountry($constraint);
        }

        /**
         * Get Job Offers
         * and make the actual repository call
         */
        $jobOffers = $this->jobOfferRepository->findByFilter(null, $filterConfig, $constraint, $order, $limit);

        /**
         * Determines whether user tried to filter
         */
        $isFiltering = false;
        if ($constraint instanceof Constraint)
        {
            $isFiltering = true;
        }

        /**
         * Assign
         */
        $this->view->assign('jobOffers', $jobOffers);
        $this->view->assign('settings',  $this->settings);
        $this->view->assign('pagination', $this->getPagination($jobOffers));
        $this->view->assign('isFiltering', $isFiltering);
        $this->view->assign('filterOptions', $filterOptions);
        $this->view->assign('constraint', $constraint);
        $this->view->assign('suggestions', $this->getAutoSuggestions());

        return $this->htmlResponse();
    }

    /**
     * action show
     *
     * @param \Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer
     * @return void
     */
    public function showAction(\Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer)
    {

        /*set structureData for GoogleForJobs */
        $g4jobs_structuredData = $this->buildG4JobsStructuredData($jobOffer);

        $this->buildMetaData($jobOffer);

        $limit =  (int)$this->settings['limit'] ?: 0;
        if(!empty($limit)){
            $this->settings['hidePagination'] = 1;
        }

        $relatedJobOffers = $this->jobOfferRepository->findRelatedJobs($jobOffer, $limit);
        $isInWatchlist = $this->jobOfferRepository->isInWatchlist($jobOffer->getUid());
        $pid = (int)($this->request->getAttribute('currentContentObject')?->data['pid'] ?? 0);

        $this->view->assignMultiple([
            'jobOffer' => $jobOffer,
            'settings' => $this->settings,
            'g4jobs_structuredData' => $g4jobs_structuredData,
            'relatedJobOffers' => $relatedJobOffers,
            'pid' => $pid,
            'isInWatchlist' => $isInWatchlist
        ]);

        return $this->htmlResponse();
    }

    /**
     * Output list view of job offers on watchlist
     *
     */
    public function watchlistAction(): ResponseInterface
    {
        /**
         * Order
         */
        $order = $this->getOrder();

        /**
         * Get Job Offers from watchlist
         */
        $jobOffers = $this->jobOfferRepository->findAllOnWatchlist($order);

        /**
         * Assign
         */
        $this->view->assign('jobOffers', $jobOffers);
        $this->view->assign('settings',  $this->settings);
        $this->view->assign('pagination', $this->getPagination($jobOffers));

        return $this->htmlResponse();
    }

    /**
     * action add JobOffer to watchlist
     *
     * @param JobOffer $jobOffer
     * @param string $redirectToAction
     * @return RedirectResponse
     */
    public function addToWatchlistAction(JobOffer $jobOffer, string $redirectToAction = 'show'): ResponseInterface
    {
        $cookie_name = "tt3career_joboffer_watchlist";
        $cookie_value = null;
        $jobOfferUid = (string)$jobOffer->getUid();
        $expiresAt = time() + (86400 * (int)$this->settings['watchlist']['cookie-lifetime']);

        // Read the cookie and decode JSON into an array
        if(isset($_COOKIE[$cookie_name])) {
            $watchlist = json_decode($_COOKIE[$cookie_name], true);
        } else {
            $watchlist = [];
        }

        // Check if the UID already exists in the watchlist
        if (($key = array_search($jobOfferUid, $watchlist)) !== false) {
            // If the UID exists, remove it
            unset($watchlist[$key]);
            // Re-index the array to avoid empty keys
            $watchlist = array_values($watchlist);
        } else {
            // If the UID does not exist, add it to the watchlist
            $watchlist[] = $jobOfferUid;
        }

        // If the watchlist is empty after removal, delete the cookie
        if (empty($watchlist)) {
            setcookie($cookie_name, '', time() - 3600, '/'); // Delete the cookie
        } else {
            // Convert the watchlist array back to JSON and save it in the cookie
            $cookie_value = json_encode($watchlist);
            setcookie($cookie_name, $cookie_value, $expiresAt, '/');
        }

        // Update the cookie value in the current script to ensure consistency
        $_COOKIE[$cookie_name] = $cookie_value;

        $currentArguments = $this->request->getArguments();
        unset($currentArguments['redirectToAction']);

        if($redirectToAction === 'show'){
            $arguments = [
                'tx_tt3career_careershow' => [
                    'controller' => 'JobOffer',
                    'action' => 'show',
                    'jobOffer' => $jobOffer->getUid(),
                ],
            ];
        } else if ($redirectToAction === 'list') {
            $arguments = [
                'tx_tt3career_careerlist' => [
                    'controller' => 'JobOffer',
                    'action' => 'list'
                ]
            ];
        } else if ($redirectToAction === 'watchlist') {
            $arguments = [
                'tx_tt3career_careerwatchlist' => [
                    'controller' => 'JobOffer',
                    'action' => 'watchlist'
                ]
            ];
        }

        $uriSection = '';
        if(isset($currentArguments['section'])) {
            $uriSection = $currentArguments['section'];
        }

        $pageArguments = $this->request->getAttribute('routing');
        $pageId = $pageArguments->getPageId();

        $uri = $this->uriBuilder->reset()
            ->setTargetPageUid($pageId) //Set current page as target
            ->setArguments($arguments)
            ->setSection($uriSection)
            ->build();

        return new RedirectResponse($uri);
    }

    /**
     * action perform (called after submitting the form)
     *
     * @param JobOffer $jobOffer
     * @return ForwardResponse
     */
    public function performAction(JobOffer $jobOffer): ResponseInterface
    {
        return new ForwardResponse('show');
    }

    /**
     * action of watchlist Button
     */
    public function watchlistButtonAction(): ResponseInterface
    {
        $jobOffers = $this->jobOfferRepository->findAllOnWatchlist();
        $amount = count($jobOffers);

        $this->view->assign('settings',  $this->settings);
        $this->view->assign('amount',  $amount);

        return $this->htmlResponse();
    }

    /**
     * Helper Funcion to return order config
     *
     * @return array
     */
    private function getOrder()
    {
        if(isset( $this->settings['orderBy'])) {
            $orderBy = $this->settings['orderBy'] ?: 'sorting';
        } else {
            $orderBy = 'sorting';
        }
        if(isset( $this->settings['orderDirection'])) {
            $orderDirection = $this->settings['orderDirection'] ?: \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
        } else {
            $orderDirection =\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
        }
        return [$orderBy => strtoupper($orderDirection)];
    }

    /**
     * Helper Funcion to return pagination config
     *
     * @param array|QueryResultInterface $jobOffers
     * @return array
     */
    private function getPagination($jobOffers)
    {
        //pagination
        $paginationConfiguration = $this->settings['list']['paginate'] ?? [];
        $itemsPerPage = (int)(($paginationConfiguration['itemsPerPage'] ?? '') ?: 9);
        $maximumLinks = (int)($paginationConfiguration['maximumLinks'] ?? 3);
        $currentPage = max(1, $this->request->hasArgument('currentPage') ? (int)$this->request->getArgument('currentPage') : 1);

        //fix showing more items if itemsPerPage > limit
        if(!empty($limit) && $limit < $itemsPerPage) {
            $itemsPerPage = $limit;
        }

        $paginator = new QueryResultPaginator(
            $jobOffers,
            $currentPage,
            $itemsPerPage,
        );
        $pagination = new SlidingWindowPagination(
            $paginator,
            $maximumLinks
        );

        return [
            'currentPage' => $currentPage,
            'pagination' => $pagination,
            'paginator' => $paginator,
        ];
    }

    /**
     * Helper Funcion to split text
     *
     * @param string $text
     * @param string $delimiter
     * @return array
     */
    private function splitText($text, $delimiter = '/')
    {
        // replacee '(m/w/d)'
        $text = preg_replace('/\s*\(m\/w\/d\)\s*/', '', $text);

        $parts = explode($delimiter, $text);
        $parts = array_map('trim', $parts);

        return $parts;
    }

    /**
     * Set all selected filter Array
     */
    private function setAllSelectedFilterArrays(): void
    {
        $this->setSelectedFilterArray();
        $this->setSelectedPreFilterArray();
        $this->allSelectedFilter = array_unique(array_merge($this->selectedFilter, $this->selectedPreFilter));
    }

    /**
     * Set selected prefilter Array
     */
    private function setSelectedPreFilterArray(): void
    {
        $selectedPreFiltersArray = null;
        if(isset($this->settings['prefilter'])){
            $selectedPreFilters = $this->settings['prefilter'];
            if (!empty($selectedPreFilters)) {
                foreach ($selectedPreFilters as $key => $value) {
                    $selectedPreFiltersArray[] = $key;
                }
                $this->selectedPreFilter = $selectedPreFiltersArray;
            }
        }
    }

    /**
     * Set selected filter Array
     */
    private function setSelectedFilterArray(): void
    {
        $selectedFiltersArray = null;
        if(isset($this->settings['filter'])){
            $selectedFilter = $this->settings['filter'];
            if (!empty($selectedFilter)) {
                $selectedFiltersArray = array_map('trim', explode(',', $selectedFilter));
                $this->selectedFilter = $selectedFiltersArray;
            }
        }
    }

    /**
     * Get filter from tsconfig based on all selected Filter
     *
     * @return array
     */
    private function getSelectedFilterOptions(): array
    {
        if (empty($this->allSelectedFilter)) {
            return [];
        }

        // Get filter from tsconfig based on selected Filter
        $contentObj = $this->request->getAttribute('currentContentObject');

        if ($contentObj === null)
        {
            throw new \RuntimeException("Could not retrieve content object. Make sure to call this with a plugin.");
        }
        $pageId = (int)$contentObj->data['pid'];

        $availableFilters = $this->filter->getAvailableFilter($pageId);

        // get only selected filter and remove dots from keys in availableFilters (caused by tsconfig)
        $filters = [];
        foreach ($this->allSelectedFilter as $selectedKey) {
            $newKey = rtrim($selectedKey, '.');
            if (array_key_exists($newKey . '.', $availableFilters)) {
                $filters[$newKey] = $availableFilters[$newKey . '.'];
            }
        }

        return $filters;
    }

    /**
     * @return array
     * @throws InvalidQueryException
     */
    private function getCachedFilterOptions(): array
    {
        $contentObj = $this->request->getAttribute('currentContentObject');
        if ($contentObj === null)
        {
            throw new \RuntimeException("Could not retrieve content object. Make sure to call this with a plugin.");
        }

        $pageId = $contentObj->data['pid'];
        if (!MathUtility::canBeInterpretedAsInteger($pageId))
        {
            throw new \RuntimeException("Page id $pageId is not valid.");
        }

        $cacheKey = "filterOptions-$pageId";

        /** @var FrontendInterface $cache */
        $cache = GeneralUtility::makeInstance(CacheManager::class)?->getCache('tt3career');

        /** @var LanguageAspect $languageAspect */
        $languageAspect = GeneralUtility::makeInstance(Context::class)?->getAspect('language');
        $languageId = $languageAspect->getId();

        // If $entry is false, or language key does not exist it hasn't been cached. Calculate the value and store it in the cache:
        if (($entry = $cache->get($cacheKey)) === false || !key_exists($languageId, $entry))
        {
            $entry = $this->getFilterOptions($languageId);

            $cache->set($cacheKey, $entry, [], null);
        }

        return $entry[$languageId];
    }

    /**
     * This function makes calls to repositories to get all available filter options.
     * These then get cached for performance reasons.
     * Override for customization.
     *
     * @param $languageId int
     *
     * @return array
     * @throws InvalidQueryException
     * @throws Exception
     */
    public function getFilterOptions($languageId): array
    {
        $filteredOptions = [];
        foreach ($this->selectedFilter as $filter) {
            switch ($filter) {
                case 'employmentType':
                    $filteredOptions['employmentType'] = $this->jobOfferRepository->findAllEmploymentTypes($languageId);
                    break;
                case 'careerLevel':
                    $filteredOptions['careerLevel'] = $this->jobOfferRepository->findAllCareerLevels($languageId);
                    break;
                case 'company':
                    $filteredOptions['company'] = $this->jobOfferRepository->findAllCompanies($languageId);
                    break;
                case 'department':
                    $filteredOptions['department'] = $this->jobOfferRepository->findAllDepartments($languageId);
                    break;
                case 'location':
                    $filteredOptions['location'] = $this->jobOfferRepository->findAllLocations($languageId);
                    break;
                case 'country':
                    $filteredOptions['country'] = $this->jobOfferRepository->findAllCountrys($languageId);
                    break;
                case 'sword':
                    $filteredOptions['sword'] = '';
                    break;
                default:
                    // Optional: handle unknown filter types if necessary
                    break;
            }
        }

        return [$languageId => $filteredOptions];
    }

    /**
     * @param Constraint|null $constraint
     *
     * @return Constraint
     */
    private function getPreFilterCareerLevel(Constraint $constraint = null): Constraint
    {
        $prefilter = (string)($this->settings['prefilter']['careerLevel'] ?? '');
        $prefilter = array_map('trim', explode(',', $prefilter));

        if ($constraint === null)
        {
            $constraint = new Constraint();
            $constraint->setCareerLevel($prefilter);
        } else
        {
            if($constraint->getCareerLevel()) {
                $constraint->setCareerLevel($constraint->getCareerLevel());
            } else if ($prefilter) {
                $constraint->setCareerLevel($prefilter);
            }
        }

        return $constraint;
    }

    /**
     * @param Constraint|null $constraint
     *
     * @return Constraint
     */
    private function getPreFilterCompany(Constraint $constraint = null): Constraint
    {
        $prefilter = (string)($this->settings['prefilter']['company'] ?? '');
        $prefilter = array_map('trim', explode(',', $prefilter));

        if ($constraint === null)
        {
            $constraint = new Constraint();
            $constraint->setCompany($prefilter);
        } else
        {
            if($constraint->getCompany()) {
                $constraint->setCompany($constraint->getCompany());
            } else if ($prefilter) {
                $constraint->setCompany($prefilter);
            }
        }

        return $constraint;
    }

    /**
     * @param Constraint|null $constraint
     *
     * @return Constraint
     */
    private function getPreFilterDepartment(Constraint $constraint = null): Constraint
    {
        $prefilter = (string)($this->settings['prefilter']['department'] ?? '');
        $prefilter = array_map('trim', explode(',', $prefilter));

        if ($constraint === null)
        {
            $constraint = new Constraint();
            $constraint->setDepartment($prefilter);
        } else
        {
            if($constraint->getDepartment()) {
                $constraint->setDepartment($constraint->getDepartment());
            } else if ($prefilter) {
                $constraint->setDepartment($prefilter);
            }
        }

        return $constraint;
    }

    /**
     * @param Constraint|null $constraint
     *
     * @return Constraint
     */
    private function getPreFilterLocation(Constraint $constraint = null): Constraint
    {
        $prefilter = (string)($this->settings['prefilter']['location'] ?? '');
        $prefilter = array_map('trim', explode(',', $prefilter));

        if ($constraint === null)
        {
            $constraint = new Constraint();
            $constraint->setLocation($prefilter);
        } else
        {
            if($constraint->getLocation()) {
                $constraint->setLocation($constraint->getLocation());
            } else if ($prefilter) {
                $constraint->setLocation($prefilter);
            }
        }

        return $constraint;
    }

    /**
     * @param Constraint|null $constraint
     *
     * @return Constraint
     */
    private function getPreFilterCountry(Constraint $constraint = null): Constraint
    {
        $prefilter = (string)($this->settings['prefilter']['country'] ?? '');
        $prefilter = array_map('trim', explode(',', $prefilter));

        if ($constraint === null)
        {
            $constraint = new Constraint();
            $constraint->setCountry($prefilter);
        } else
        {
            if($constraint->getCountry()) {
                $constraint->setCountry($constraint->getCountry());
            } else if ($prefilter) {
                $constraint->setCountry($prefilter);
            }
        }

        return $constraint;
    }

    /**
     * @var \Vendor\Extension\Domain\Repository\JobOfferRepository
     * @return array
     */
    protected function getAutoSuggestions(): array {

        $jobOffers = $this->jobOfferRepository->findAllForAutoSuggestion();

        $suggestions = [];
        foreach ($jobOffers as $jobOffer) {
            if (!empty($jobOffer['title'])) {
                $suggestions[] = trim($jobOffer['title']);
            }
            if (!empty($jobOffer['subtitle'])) {
                $suggestions[] = trim($jobOffer['subtitle']);
            }
            if (!empty($jobOffer['meta_keywords'])) {
                $keywords = explode(',', $jobOffer['meta_keywords']);
                foreach ($keywords as $keyword) {
                    $suggestions[] = trim($keyword);
                }
            }
        }

        $suggestions = array_unique($suggestions);
        sort($suggestions);

        return $suggestions;
    }

    /**
     * @param \Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer
     */
    protected function buildG4JobsStructuredData(\Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer)
    {
        $settings = $this->settings;

        $structuredData = null;
        $jobOffer->getGoogleforjobs()->rewind();
        $oGoogleForJobs = $jobOffer->getGoogleforjobs()->current();

        if ($oGoogleForJobs) {
            $aGoogleForJobs = [];
            $aGoogleForJobs['@context'] = 'https://schema.org/';
            $aGoogleForJobs['@type'] = 'JobPosting';

            //Directly Apply
            if(isset($settings['googleforjobs'])) {
                if($settings['googleforjobs']['directapply'] == '1'){
                    $aGoogleForJobs['directApply'] = true;
                }
            }

            //Title & Subtitle
            $aGoogleForJobs['title'] = $oGoogleForJobs->getTitle() .' '. $oGoogleForJobs->getSubtitle();
            if ($oGoogleForJobs->getTitle() == '') {
                $aGoogleForJobs['title'] = $jobOffer->getTitle() .' '. $jobOffer->getSubtitle();
            }

            //Description
            $aGoogleForJobs['description'] = $oGoogleForJobs->getDescription();
            if ($oGoogleForJobs->getDescription() == '') {
                $desc = $jobOffer->getIntro() . $jobOffer->getTask() . $jobOffer->getProfile() . $jobOffer->getBenefits() . $jobOffer->getInformation();
                $aGoogleForJobs['description'] = $desc;
            }

            //Image
            if ($oGoogleForJobs->getHiringorganizationLogo() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['image'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];
                }
            } else {
                $aGoogleForJobs['image'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}" . '/' . $oGoogleForJobs->getHiringorganizationLogo()->getOriginalResource()->getPublicUrl();
            }

            //Employment Type
            $aGoogleForJobs['employmentType'] = explode(",", $oGoogleForJobs->getEmploymenttype());
            $aGoogleForJobs['employmentType'] = $oGoogleForJobs->getEmploymenttype();
            if ($oGoogleForJobs->getEmploymenttype() == '') {
                $aGoogleForJobs['employmentType'] = explode(",", $jobOffer->getEmploymenttype());
            }

            //Identifier
            $aGoogleForJobs['identifier']['@type'] = 'PropertyValue';
            $aGoogleForJobs['identifier']['name'] = $oGoogleForJobs->getIdentifierName();
            if ($oGoogleForJobs->getIdentifierName() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['identifier']['name'] = $this->settings['googleforjobs']['identifier']['name'];
                }
            }
            $aGoogleForJobs['identifier']['value'] = $oGoogleForJobs->getIdentifierValue();
            if ($oGoogleForJobs->getIdentifierValue() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['identifier']['value'] = $this->settings['googleforjobs']['identifier']['value'];
                }
            }

            //hiringOrganization
            $aGoogleForJobs['hiringOrganization']['@type'] = 'Organization';
            $aGoogleForJobs['hiringOrganization']['name'] = $oGoogleForJobs->getHiringorganizationName();
            if ($oGoogleForJobs->getHiringorganizationName() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['hiringOrganization']['name'] = $this->settings['googleforjobs']['hiringOrganization']['name'];
                }
                if ($aGoogleForJobs['hiringOrganization']['name'] == '') {
                    $aGoogleForJobs['hiringOrganization']['name'] = $jobOffer->getCompany();
                }
            }
            $aGoogleForJobs['hiringOrganization']['sameAs'] = $oGoogleForJobs->getHiringorganizationWebsite();
            if ($oGoogleForJobs->getHiringorganizationWebsite() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['hiringOrganization']['sameAs'] = $this->settings['googleforjobs']['hiringOrganization']['website'];
                }
            }
            if ($oGoogleForJobs->getHiringorganizationLogo() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['hiringOrganization']['logo'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];
                }
            } else {
                $aGoogleForJobs['hiringOrganization']['logo'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}" . '/' . $oGoogleForJobs->getHiringorganizationLogo()->getOriginalResource()->getPublicUrl();
            }

            //jobLocation
            $aGoogleForJobs['jobLocation']['@type'] = 'Place';
            $aGoogleForJobs['jobLocation']['address']['@type'] = 'PostalAddress';
            $aGoogleForJobs['jobLocation']['address']['streetAddress'] = $oGoogleForJobs->getJoblocationAddressStreet();
            if ($oGoogleForJobs->getJoblocationAddressStreet() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['jobLocation']['address']['streetAddress'] = $this->settings['googleforjobs']['jobLocation']['address']['street'];
                }
            }
            $aGoogleForJobs['jobLocation']['address']['addressLocality'] = $oGoogleForJobs->getJoblocationAddressCity();
            if ($oGoogleForJobs->getJoblocationAddressCity() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['jobLocation']['address']['addressLocality'] = $this->settings['googleforjobs']['jobLocation']['address']['city'];
                }
            }
            $aGoogleForJobs['jobLocation']['address']['addressRegion'] = $oGoogleForJobs->getJoblocationAddressRegion();
            if ($oGoogleForJobs->getJoblocationAddressRegion() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['jobLocation']['address']['addressRegion'] = $this->settings['googleforjobs']['jobLocation']['address']['region'];
                }
            }
            $aGoogleForJobs['jobLocation']['address']['postalCode'] = $oGoogleForJobs->getJoblocationAddressPostalcode();
            if ($oGoogleForJobs->getJoblocationAddressPostalcode() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['jobLocation']['address']['postalCode'] = $this->settings['googleforjobs']['jobLocation']['address']['postalCode'];
                }
            }
            $aGoogleForJobs['jobLocation']['address']['addressCountry'] = $oGoogleForJobs->getJoblocationAddressCountry();
            if ($oGoogleForJobs->getJoblocationAddressCountry() == '') {
                if(isset($settings['googleforjobs'])) {
                    $aGoogleForJobs['jobLocation']['address']['addressCountry'] = $this->settings['googleforjobs']['jobLocation']['address']['country'];
                }
            }

            //baseSalary
            if ($oGoogleForJobs->getBasesalaryValue() > 0) {
                $aGoogleForJobs['baseSalary']['@type'] = 'MonetaryAmount';
                $aGoogleForJobs['baseSalary']['currency'] = $oGoogleForJobs->getBasesalaryCurrency();
                $aGoogleForJobs['baseSalary']['value']['@type'] = 'QuantitativeValue';
                $aGoogleForJobs['baseSalary']['value']['value'] = $oGoogleForJobs->getBasesalaryValue();
                $aGoogleForJobs['baseSalary']['value']['unitText'] = $oGoogleForJobs->getBasesalaryUnit();
            }

            //remoteJob
            if ($oGoogleForJobs->isRemotejob()) {
                $aGoogleForJobs['jobLocationType'] = 'TELECOMMUTE';
                $aGoogleForJobs['applicantLocationRequirements']['@type'] = 'Country';
                $aGoogleForJobs['applicantLocationRequirements']['name'] = $oGoogleForJobs->getRemotejobLocationRequirements();
            }

            //datePosted
            if ($oGoogleForJobs->getDatePosted()) {
                $aGoogleForJobs['datePosted'] = $oGoogleForJobs->getDatePosted()->format('Y-m-d');
            } else {
                //use current date
                $aGoogleForJobs['datePosted'] = date('c');
            }

            //validThrough
            if ($oGoogleForJobs->getValidThrough()) {
                $aGoogleForJobs['validThrough'] = $oGoogleForJobs->getValidThrough()->format('Y-m-d');
            }

            // make json
            $jsonData = json_encode($aGoogleForJobs, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

            // add script tag
            $structuredData = '<script type="application/ld+json">' . $jsonData . '</script>';
        } else {
            $aGoogleForJobs = [];
            $aGoogleForJobs['@context'] = 'https://schema.org/';
            $aGoogleForJobs['@type'] = 'JobPosting';

            //Directly Apply
            if (isset($settings['googleforjobs'])) {
                if($settings['googleforjobs']['directapply'] == '1'){
                    $aGoogleForJobs['directApply'] = true;
                }
            }

            //Title & Subtitle
            $aGoogleForJobs['title'] = $jobOffer->getTitle() .' '. $jobOffer->getSubtitle();

            //Description
            $aGoogleForJobs['description'] = $jobOffer->getIntro() . $jobOffer->getTask() . $jobOffer->getProfile() . $jobOffer->getBenefits() . $jobOffer->getInformation();

            //Image
            if (isset($settings['googleforjobs'])) {
                $aGoogleForJobs['image'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];
            }

            //Employment Type
            $aGoogleForJobs['employmentType'] = explode(",", $jobOffer->getEmploymenttype());

            //Identifier
            $aGoogleForJobs['identifier']['@type'] = 'PropertyValue';
            $aGoogleForJobs['identifier']['name'] = 'tt3career JobOffer ID';
            $aGoogleForJobs['identifier']['value'] = $jobOffer->getUid();

            //hiringOrganization
            if (isset($settings['googleforjobs'])) {
                $aGoogleForJobs['hiringOrganization']['@type'] = 'Organization';
                $aGoogleForJobs['hiringOrganization']['name'] = $this->settings['googleforjobs']['hiringOrganization']['name'];
                $aGoogleForJobs['hiringOrganization']['sameAs'] = $this->settings['googleforjobs']['hiringOrganization']['website'];
                $aGoogleForJobs['hiringOrganization']['logo'] = $this->settings['googleforjobs']['hiringOrganization']['logo'];
            }

            //jobLocation
            if (isset($settings['googleforjobs'])) {
                if (isset($settings['googleforjobs']['jobLocation'][0])) {
                    $aGoogleForJobs['jobLocation'] = [];
                    foreach ($settings['googleforjobs']['jobLocation'] as $jobLocation) {
                        $aGoogleForJobs['jobLocation'][] = [
                            '@type' => 'Place',
                            'address' => [
                                '@type' => 'PostalAddress',
                                'streetAddress' => $jobLocation['address']['street'] ?? '',
                                'addressLocality' => $jobLocation['address']['city'] ?? '',
                                'addressRegion' => $jobLocation['address']['region'] ?? '',
                                'postalCode' => $jobLocation['address']['postalCode'] ?? '',
                                'addressCountry' => $jobLocation['address']['country'] ?? '',
                            ],
                        ];
                    }
                } else {
                    $aGoogleForJobs['jobLocation'] = [
                        '@type' => 'Place',
                        'address' => [
                            '@type' => 'PostalAddress',
                            'streetAddress' => $this->settings['googleforjobs']['jobLocation']['address']['street'] ?? '',
                            'addressLocality' => $this->settings['googleforjobs']['jobLocation']['address']['city'] ?? '',
                            'addressRegion' => $this->settings['googleforjobs']['jobLocation']['address']['region'] ?? '',
                            'postalCode' => $this->settings['googleforjobs']['jobLocation']['address']['postalCode'] ?? '',
                            'addressCountry' => $this->settings['googleforjobs']['jobLocation']['address']['country'] ?? '',
                        ],
                    ];
                }
            }

            //datePosted
            $aGoogleForJobs['datePosted'] = date('c');

            // make json
            $jsonData = json_encode($aGoogleForJobs, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

            // add script tag
            $structuredData = '<script type="application/ld+json">' . $jsonData . '</script>';
        }
        return $structuredData;
    }

    /**
     * @return void
     * @throws InvalidQueryException
     * @throws NoSuchArgumentException
     */
    protected function buildMetaData(\Teufels\Tt3Career\Domain\Model\JobOffer $jobOffer)
    {
        //MetaData Settings
        $sMetaTitle = $jobOffer->getMetaTitle();
        $sMetaDescription = $jobOffer->getMetaDescription();
        $sMetaKeywords = $jobOffer->getMetaKeywords();

        //Open Graph Settings
        $sOgTitle = $jobOffer->getOgTitle();
        $sOgDescription = $jobOffer->getOgDescription();
        $sOgImage = $jobOffer->getOgImage();
        if($sOgImage != ''){
            $sOgImagePath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}" . '/' . $jobOffer->getOgImage()->getOriginalResource()->getPublicUrl();
        }

        //Twitter Settings
        $sTwitterCard = $jobOffer->getTwitterCard();
        $sTwitterTitle = $jobOffer->getTwitterTitle();
        $sTwitterDescription = $jobOffer->getTwitterDescription();
        $sTwitterImage = $jobOffer->getTwitterImage();
        if ($sTwitterImage != ''){
            $sTwitterImagePath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}" . '/' . $jobOffer->getTwitterImage()->getOriginalResource()->getPublicUrl();
        }

        //Meta Description
        if ($sMetaDescription != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('description');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('description', $sMetaDescription);
        }
        //Meta Keywords
        if ($sMetaKeywords != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('keywords');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('keywords', $sMetaKeywords);
        }

        //Open Graph
        if ($sOgTitle != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:title');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('og:title', $sOgTitle);
        }
        if ($sOgDescription != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:description');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('og:description', $sOgDescription);
        }
        if ($sOgImage != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:image');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('og:image', $sOgImagePath);
        }

        //Twitter
        if ($sTwitterCard != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('twitter:card');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('twitter:card', $sTwitterCard);
        }
        if ($sTwitterTitle != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('twitter:title');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('twitter:title', $sTwitterTitle);
        }
        if ($sTwitterDescription != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('twitter:description');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('twitter:description', $sTwitterDescription);
        }
        if ($sTwitterImage != '') {
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('twitter:image');
            // @extensionScannerIgnoreLine
            $metaTagManager->addProperty('twitter:image', $sTwitterImagePath);
        }
    }

}