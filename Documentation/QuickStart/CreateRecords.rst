.. _quickContent:
.. _howToStart:

===========================
Create some initial content
===========================

.. _quickPageStructure:

Recommended page structure
==========================

Create at least the following pages:

*  "Home": Root page of the site, containing the root TypoScript template record and
   the start page content: Normal page.
*  "Jobs": A folder to store the job offers in: Folder

Usually you will also need

*  "Job list": A list page to display all job offers on: Normal page
*  "Job watchlist": A list page to display the job offers on the watchlist on: Normal page
*  "Job detail": A single page to display the job offer detail view on:
   Normal page, hidden in menu
*  "ContactPerson": A folder to store the job offer contact persons in: Folder

Your page tree could, for example look like that:

.. code-block:: none

   Home
   ├── Some page
   ├── ...
   ├── Job list
   │  └── Job detail
   ├── Job watchlist
   ├── ...
   └── RECORDS
      ├── Other storage
      ├── ...
      └── Career
        └── Jobs
        └── ContactPerson

..  directory-tree::

    *   Home

        *   :file:`Somepage`
        *   :file:`...`
        *   :file:`Job list`

            *   :file:`Job detail`

        *   :file:`Job watchlist`
        *   :file:`...`
        *   :path:`RECORDS`

            *   :path:`Other storage`
            *   :path:`...`

            *   :path:`Career`

                *   :path:`Jobs`
                *   :path:`ContactPerson`

.. _quickJobOfferRecords:
.. _howToStartCreateRecords:

Create job offer records
===================

Before any job offer record can be shown in the frontend those need to be
created.

#. Go to the module :guilabel:`Web > List`

#. Go to the "Job Storage" Folder that you created in the first step.

#. Use the icon in the topbar :guilabel:`Create new record` and select the
   entry :guilabel:`Job Offer`.

#. Fill out all desired fields and click :guilabel:`Save`.

More information about the records can be found here:
:ref:`job record <recordJob>`. (tbd)


.. _howToStartAddPlugin:

Add plugins: display the job offers in the frontend
=============================================

A plugin is used to render a defined selection of records in the frontend.
Follow these steps to add a plugin respectively for detail and list view to
a page:

Detail page
-----------

#. Go to module :guilabel:`Web > Page` and to the previously created page
   "Job detail".

#. Add a new content element and select the entry
   :guilabel:`teufels Career > Job offer detail view`.

#. Switch to the tab :guilabel:`Plugin` where you can define the plugins settings.

   #. Unter the tab :guilabel:`Additional` it is possible to define the amount of Records show in the related job list on the detail page.

   #. Unter the tab :guilabel:`Template` it is possible to select the templates which should be used for the detail. see :ref:`Templating <Templates>`.

   #. Save the plugin.

List page
---------

.. include:: /Images/AutomaticScreenshots/Plugin.rst.txt

#. Go to module :guilabel:`Web > Page` and to the previously created page
   "Job list".

#. Add a new content element and select the entry
   :guilabel:`teufels Career > Job offer list`.

#. Switch to the tab :guilabel:`Plugin` where you can define the plugins settings.
   The most important settings are :guilabel:`PageId for single job offer display` and :guilabel:`Record Storage Page`.

   #. Fill the field :guilabel:`PageId for single job offer display` with the detail page you just created before.

   #. Fill the field :guilabel:`Startingpoint` by selecting the :guilabel:`sysfolder` you created
      for the job records.

   #. Unter the tab :guilabel:`Additional` it is possible to define the amount of Records show in the job list.
      How many jobs per page should be displayed or it is possible to deactivate the pagination at all.

   #. Unter the tab :guilabel:`Sort order` it is possible to define the sort order for the listing.

   #. Unter the tab :guilabel:`Filter` it is possible to select the filters to be available in frontend or define the values for pre-filtering.
      The order of the :guilabel:`Filter` is also taken into account in the frontend output

   #. Unter the tab :guilabel:`Template` it is possible to select the templates which should be used for the list. see :ref:`Templating <Templates>`.

   #. Save the plugin.


Have a look at the frontend
===========================

Load the "Job list" page in the frontend and you should now see the job offer records
as output. A click on the title should show the job record on the
detail page. You want to change the way the records are displayed? Have a look
at the chapter :ref:`Templating <Templates>`
