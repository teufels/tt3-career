.. _quickConfiguration:

===================
Quick configuration
===================

Include TypoScript template
===========================

It is necessary to include at least the basic TypoScript provided by this
extension.

Go module :guilabel:`Web > Template` and chose your root page.
It should already contain the Sitepackage TypoScript template record.
Switch to view
:guilabel:`Info/Modify` and click on :guilabel:`Edit the whole template record`.

.. figure:: /Images/Configuration/Tt3CareerIncludeTypoScript.png
   :class: with-shadow

Switch to tab :guilabel:`Includes` and add the following templates from the list
to the right:
- :guilabel:`[teufels] Career (tt3_carrer)`.

add them above the :guilabel:`[teufels] Sitepackage (sitepackage)`.

Read more about possible configurations via TypoScript in the
:ref:`Reference <typoscript>` section.
