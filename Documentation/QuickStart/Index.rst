.. _quickStart:

===========
Quick start
===========

.. rst-class:: bignums-tip

#. Install this extension:

   .. code-block:: bash

      composer require teufels/tt3-career

   .. rst-class:: horizbuttons-attention-m

   -  :ref:`Quick installation <quickInstallation>`

#. Configuration:

   -  Include the TypoScript template
   -  Further configuration

   .. rst-class:: horizbuttons-attention-m

   -  :ref:`Quick configuration <quickConfiguration>`

#. Create initial content:

   -  Recommended page structure
   -  Create job offer records

   .. rst-class:: horizbuttons-attention-m

   -  :ref:`Recommended page structure <quickPageStructure>`
   -  :ref:`Create job offer records <quickJobOfferRecords>`


.. note::
   If your planning to use the application form, create a secure file storage, that is not accessible from the web (if not present already).
   You can read here how to configure it and set it up: :ref:`Filestorage for applications <filestorage>`.


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :hidden:

   Installation
   Configuration
   CreateRecords
