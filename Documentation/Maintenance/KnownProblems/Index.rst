..  include:: /Includes.rst.txt

..  _known-problems:

==============
Known problems
==============

* As of TYPO3 11.5.35, the absolute URL for secure_fileadmin must be entered in the lookRootPath variable (best by adjusting the AdditionalConfiguration.php + adding the path to the staging.ini/production.ini)
  * https://bitbucket.org/teufels/erle-23-ws-installer/src/custom/web/typo3conf/AdditionalConfiguration.php
  * https://bitbucket.org/teufels/boilerplate-t3/commits/cb48d4175b337896c4fb69680711cda581262657

Find a list of `open issues on Bitbucket
<https://bitbucket.org/teufels/tt3-career/jira>`__.
