.. _changelog:

=========
Changelog
=========

This changelog contains only the major changes

The full changelog can be found here: `CHANGELOG <https://bitbucket.org/teufels/tt3-career/src/main/CHANGELOG.md>`__

List of versions
================

.. toctree::
    :maxdepth: 5
    :titlesonly:
    :glob:

    2-0-0
    1-0-0
