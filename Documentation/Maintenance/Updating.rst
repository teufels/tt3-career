.. _updating:

========
Updating
========

.. contents:: Table of contents
    :depth: 2
    :local:

Versioning
==========
EXT:tt3_career follows the `semantic versioning <https://semver.org/>`__ approach.

It uses a 3-number versioning scheme: *<major>.<minor>.<patch>*

- Major: Breaking changes
- Minor: New functionality, backwards compatible (without breaking changes)
- Patch: Bug fixes (without breaking changes)


Before an update
================

Before you start the update procedure, please read the changelog of all versions which have been
released in the meantime! You can find those in the :ref:`CHANGELOG <changelog>`.

Furthermore, it is **always** a good idea to do updates on a dedicated test installation or at least create a database backup.


Update previous versions
========================

In general after every update you should update the database scheme in :guilabel:`Admin tools > Maintenance > Analyze Database` and flush all caches.

Updating from 1.x to 2.0.0
--------------------------
..  versionadded:: 2.0
    Version 2.0 brings a lot of new features and enhancements. For a more detailed overview see :ref:`CHANGELOG <changelog>`

..  attention::
    The implementation of the application form in the detail view is deactivated by default with version 2.0 and must be activated if necessary, if the detail template has not been overwritten anyway.

..  tip::
    If you want to use the new features check this documentation.

Updating from 0.x to 1.0.0
--------------------------
..  versionchanged:: 1.0
    With version 1.0.0 multi-file upload come back to the functionality.
    After updating check & test existing application formular.

..  tip::
    If you want to use multifile upload include TypoScript Template `form_extended`
    and enable checkbox `Enable multiple file upload` for upload field(s).
    Test form, form log and mail delivery.

Updating from hive_career to tt3_career
---------------------------------------
..  attention::
    To Update and migrate from hive_career to tt3_career see :ref:`MIGRATION <migration>`
