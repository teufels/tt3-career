.. _migration:

==================
Migration
==================

.. contents:: Table of contents
    :depth: 2
    :local:

Update & Migration from hive_career
===================================

This tutorial will help you to migrate data from hive_career to tt3_career.

Before migration
----------------
Before you start the migration procedure it is **always** a good idea to do migration on a dedicated test installation first or at least create a database backup.

Requirements
------------
- Installed extension `beewilly/hive_career`

Migration
---------
..  rst-class:: bignums

1. in composer.json replace `"beewilly/hive_career"` with `"teufels/tt3-career"` check for latest version
2. Composer update
3. Include TypoScript set :guilabel:`[teufels] Career`
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_career yet)
5. Perform Upgrade Wizards :guilabel:`[teufels] Career`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_career now)
7. class & id changed -> adjust styling in `sitepackage` (e.g. `tx-hive-career` => `tx-tt3-career`)
8. merge TS :file:`Setup/Constants` overrides -> adjust TS :file:`Setup/Constants` overrides in `sitepackage`
9. merge :file:`Templates/Partials/Layout` overrides -> adjust :file:`Templates/Partials/Layout` overrides in `sitepackage`
10. check & adjust be user group access rights
11. Plugins may need to be saved again once
12. adjust site-config for speaking URL. `[snippet] <https://bitbucket.org/teufels/workspace/snippets/aqLaqr>`__.
13. Test all
