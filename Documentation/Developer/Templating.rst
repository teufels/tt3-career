.. _templating:

==========
Templating
==========

EXT:tt3_career is using Fluid as templating engine.

..  seealso::
    This documentation won't bring you all information about Fluid but only the
    most important things you need for using it. You can get
    more information in the section :ref:`Fluid templates of the Sitepackage tutorial
    <t3sitepackage:fluid-templates>`. A complete reference of Fluid ViewHelpers
    provided by TYPO3 can be found in the  :ref:`ViewHelper Reference <t3viewhelper:start>`

.. _templatingOverride:

Overriding templates
====================

Copy the Fluid templates that you want to adjust to your `sitepackage`.

You find the original templates in the :file:`EXT:tt3_career/Resources/Private/Templates/`
and the partials in :file:`EXT:tt3_career/Resources/Private/Partials/`.

..  attention::
    Never change these templates directly!

To override the standard career templates
with your own you can use the TypoScript **constants** to set the
paths:

.. code-block:: typoscript
   :caption: TypoScript constants

   plugin.tx_tt3career_careershow {
      view {
         templateRootPath = EXT:sitepackage/Resources/Private/Overrides/tt3_career/Templates/
         partialRootPath = EXT:sitepackage/Resources/Private/Overrides/tt3_career/Partials/
         layoutRootPath = EXT:sitepackage/Resources/Private/Overrides/tt3_career/Layouts/
      }
   }

Create the file
:file:`EXT:sitepackage/Configuration/TypoScript/Constants/ext.tt3_career.typoscript` in your
`sitepackage`,add these lines to the file and create the corresponding directories.

Copy the files you want to override from the directories. in `EXT:tt3_career` to those directories.

..  attention::
    It is strongly recommended to only copy the files you actually want to overwrite.

The same procedure applies to all plugins

.. code-block:: typoscript

    plugin.tx_tt3career_careerlist
    plugin.tx_tt3career_careerwatchlist
    plugin.tx_tt3career_careerwatchlistbutton

.. _templatingCreate:

Create own Layout templates
===========================
You can create own templates over tsconfig

1. create TT3Career.tsconfig in sitepackage & import the file in All.tscconfig
2. add own Templates for tx_tt3career.settings.templateLayouts.list or tx_tt3career.settings.templateLayouts.detail

..  code-block:: typoscript
    :caption: EXT:sitepackage/Configuration/TsConfig/Page/TT3Career.tsconfig

      tx_tt3career.settings {
        templateLayouts {
          list {
            custom = Custom List Layout
          }
          detail {
            custom = Custom Detail Layout
          }
        }
      }

It is possible to remove basis Templates with `>` in the created TT3Career.tsconfig (if it should not be selectable in the backend)

..  code-block:: typoscript
    :caption: EXT:sitepackage/Configuration/TsConfig/Page/TT3Career.tsconfig

    tx_tt3career.settings {
      templateLayouts {
        list >
        list {
          only = only available Template Layout
        }
      }
    }

