.. _filter:

======
Filter
======

Prepared filters
----------------

The extension comes with the support of following filter

- Search word (sword)
- Employment Type (employmentType)
- Carrer Level (careerLevel)
- Company (company)
- Department (department)
- Location (location)
- Country (country)

in the plugin you can select which of these filters are used in the frontend. The order is taken into account.

The filter are configurated in the :file::`Configuration/TsConfig/Page/Settings.tsconfig`

..  code-block:: typoscript
    :caption: EXT:tt3_career/Configuration/TsConfig/Page/Settings.tsconfig

    tx_tt3career.settings {
        filter {
            sword {
                label = Search word
                relation = title,subtitle,metaKeywords
                relationType = like
            }
            employmentType {
                label = Employment Type
                relation = employmenttype
                relationType = contains
            }
            careerLevel {
                label = Career Level
                relation = careerLevel
                relationType = equals
            }
            company {
                label = Company
                relation = company
                relationType = equals
            }
            department {
                label = Department
                relation = department
                relationType = equals
            }
            location {
                label = Location
                relation = location
                relationType = equals
            }
            country {
                label = Country
                relation = country
                relationType = equals
            }
        }
    }

It is possible to remove basis filter with `>` with tsconfig (if it should not be selectable in the backend)

It is recommended to create a TT3Career.tsconfig

..  code-block:: typoscript
    :caption: EXT:sitepackage/Configuration/TsConfig/Page/TT3Career.tsconfig

    tx_tt3career.settings {
      filter {
        emplyomentType >
      }
    }
