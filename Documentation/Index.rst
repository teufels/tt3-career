..  include:: /Includes.rst.txt

.. _start:

==========
tt3_career
==========

:Extension key:
    tt3_career

:Package name:
    teufes/tt3-career

:Version:
    |release|

:Language:
    en

:Author:
    `teufels GmbH <http://teufels.com>`__

:License:
    This document is published under the
    `GNU General Public License 3 <https://www.gnu.org/licenses/gpl-3.0>`__
    license.

:Rendered:
    |today|

----

This extension allows you to create and manage job listings, featuring filtering, watchlist,
detail view with optional application form or link to external one, and compatibility with Google for Jobs structured data.

..  note::
    If you find an issue in or want to contribute, you find this extension on Bitbucket:
    `tt3_career on Bitbucket <https://bitbucket.org/teufels/tt3-career/src/>`__.

----

**Table of Contents:**

..  toctree::
    :maxdepth: 2
    :titlesonly:

    Introduction/Index
    QuickStart/Index
    Configuration/Index
    Administration/Index
    Developer/Index

    Editor/Index


..  toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: MAINTENANCE

    Maintenance/Updating
    Maintenance/Migration
    Maintenance/KnownProblems/Index
    Maintenance/Changelog/Index

..  toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: REFERENCE

    Reference/GoogleForJobs/Index
    Reference/MenuProcessor/Index
    Reference/SEO/Index
    Reference/Translations/Index
    Reference/TypoScript/Index

..  toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: DEPENDENCIES

    Dependencies/Requirements
    Dependencies/Addons

..  Meta Menu

..  toctree::
    :hidden:

    Sitemap
