.. _tt3FormPost:

=====================
EXT:tt3_form_post
=====================

This extension adds a custom form finisher "HTTP POST/GET" to the
TYPO3 form framework which call plain HTTP Request (POST/GET) to transfer data via cURL.
The transmitted Data will be generated as array from the Form Fields.

Details can be found at https://bitbucket.org/teufels/tt3-form-post/src/main/

Usage
-----

- Use `composer req teufels/tt3-form-post`
