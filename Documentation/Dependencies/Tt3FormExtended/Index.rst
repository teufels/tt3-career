.. _tt3FormExtended:

=====================
EXT:tt3_form_extended
=====================

This extension is required to extend the TYPO3 form framework

- Override/Extend form & collection from based extensions
- require `wapplersystems/form_extended` for Multiple-File-Upload
- require `pagemachine/typo3-formlog` for logging usage of application form
- Overrides "EmailFinisher" & "LoggerFinisher" for Multiple-File Upload Support (provided by form_extended)
- Overrides "EmailFinisher" to add additional receiver from teufels career > JobOffer to BCC for Finisher MailToReceiver

Details can be found at https://bitbucket.org/teufels/tt3-form-extended/src/main/

Usage
-----

is required in the tt3-career :file:`composer.json` ::

    "require": {
        "teufels/tt3-form-extended": "^1.0"
    }

