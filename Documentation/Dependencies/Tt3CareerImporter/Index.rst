.. _tt3CareerImporter:

=======================
EXT:tt3_career_importer
=======================

import job offers from external sources with the external_import Extension

Details can be found at https://bitbucket.org/teufels/tt3-career-importer/src/main/

Usage
-----

- Use `composer req teufels/tt3-career-importer`

