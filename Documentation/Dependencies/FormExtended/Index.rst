.. _formExtended:

=================
EXT:form_extended
=================

This extension is required to add the multi-upload function to the EXT:tt3_career extension.

Read more about the extension on https://github.com/WapplerSystems/form_extended

Usage
-----

is required over the EXT:tt3_form_extended.
see dependencies :ref:`EXT:tt3_form_extended <tt3FormExtended>` for more info
