.. _ttAddress:

==============
EXT:tt_address
==============

This extension is required to add the contact person function to the tt3-career extension.

Read more about the extension on https://docs.typo3.org/p/friendsoftypo3/tt-address/main/en-us/

Usage
-----

is required in the tt3-career :file:`composer.json` ::

    "require": {
        "friendsoftypo3/tt-address": "^9.0"
    }
