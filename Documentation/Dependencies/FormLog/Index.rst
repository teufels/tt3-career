.. _formLog:

=================
EXT:formlog
=================

This extension is required to add form log to the EXT:tt3_career extension.

Read more about the extension on https://docs.typo3.org/p/pagemachine/typo3-formlog/main/en-us/

Usage
-----

is required over the EXT:tt3_form_extended.
see dependencies :ref:`EXT:tt3_form_extended <tt3FormExtended>` for more info
