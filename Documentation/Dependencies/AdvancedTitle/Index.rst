.. _advancedTitle:

=================
EXT:advancedtitle
=================

This extension adds page title prefix, suffix and absolute title field to the SEO tab.
And is used to output the record title as page title on detail pages.
It is included in the [teufels] sitepackage

Details can be found at https://extensions.typo3.org/extension/advancedtitle

Usage
-----

- Use `composer req t3brightside/advancedtitle`
