.. _menuProcessor:

==============
Menu Processor
==============

The extension comes with a Menu Processor :file:`Classes/DataProcessing/AddJobOfferToMenuProcessor.php`
to add the current job offer record to any menu.

..  code-block:: typoscript
    :caption: Example

    ## Breadcrumb Menu for tt3-career Detail
    22 = Teufels\Tt3Career\DataProcessing\AddJobOfferToMenuProcessor
    22.menus = breadcrumbMenu






