.. _translations:

============
Translations
============

The extension currently supports English and German.

To adapt the texts or create further translations, use the procedure recommended by TYPO3 for overwriting and extending translation files (XLF),
which can be found under `Managing translations <https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/Localization/ManagingTranslations.html>`__,
in your sitepackage.


