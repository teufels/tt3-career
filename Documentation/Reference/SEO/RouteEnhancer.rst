.. _routeEnhancer:

===========================
Use Routing to rewrite URLs
===========================

This section will show you how you can rewrite the URLs for EXT:tt3_career using
**Routing Enhancers and Aspects**. TYPO3 Explained has a chapter
:ref:`Introduction to routing <t3coreapi:routing-introduction>` that you can read
if you are not familiar with the concept yet.

..  seealso::
    See `routing documentation <https://docs.typo3.org/p/georgringer/news/main/en-us/Tutorials/BestPractice/Routing/Index.html>`__ on the news extension for a good tutorial on how to use Route Enhancer

Quick start
===========

This section shows Example of a routing covering show, list and watchlist for the quick start

.. code-block:: yaml
    :caption: :file:`/config/sites/<your_identifier>/config.yaml`
    :linenos:

    routeEnhancers:
      CareerShow:
        type: Extbase
        #limitToPages: <show uid>
        namespace: 'tx_tt3career_careershow'
        routes:
          - routePath: '/{career-title}'
            _controller: 'JobOffer::show'
            _arguments:
              career-title: jobOffer
          - routePath: '/'
            _controller: 'JobOffer::list'
        defaultController: 'JobOffer::list'
        aspects:
          career-title:
            type: PersistedAliasMapper
            tableName: tx_tt3career_domain_model_joboffer
            routeFieldName: path_segment
      CareerList:
        type: Extbase
        #limitToPages: [list uids]
        namespace: 'tx_tt3career_careerlist'
        routes:
          - routePath: '/'
            _controller: 'JobOffer::list'
          - routePath: '/{page-label}-{page}'
            _controller: 'JobOffer::list'
            _arguments:
              page: 'currentPage'
        defaultController: 'JobOffer::list'
        defaults:
          page: '1'
        requirements:
          page: '\d+'
        aspects:
          page:
            type: StaticRangeMapper
            start: '1'
            end: '100'
          page-label:
            type: LocaleModifier
            default: 'page'
            localeMap:
              - locale: 'de_DE.*'
                value: 'seite'
              - locale: 'zh_CN.*'
                value: '第'
      CareerWatchlist:
        type: Extbase
        #limitToPages: <watchlist uid>
        namespace: 'tx_tt3career_careerwatchlist'
        routes:
          - routePath: '/'
            _controller: 'JobOffer::list'
          - routePath: '/{page-label}-{page}'
            _controller: 'JobOffer::list'
            _arguments:
              page: 'currentPage'
        defaultController: 'JobOffer::list'
        defaults:
          page: '1'
        requirements:
          page: '\d+'
        aspects:
          page:
            type: StaticRangeMapper
            start: '1'
            end: '100'
          page-label:
            type: LocaleModifier
            default: 'page'
            localeMap:
              - locale: 'de_DE.*'
                value: 'seite'
              - locale: 'zh_CN.*'
                value: '第'

Troubleshooting
---------------

*   Did you save the site configuration file?
*   Did you delete all caches?
*   In the format YAML indentation matters. The code above **must** be indentated exactly
    as shown, the keyword `routeEnhancers` **must not** be indeted.
*   The configuration above has comment out `limitTpPages` if you using it,
    did you put the correct pid of page containing the job plugins?
