.. _seo:

===
SEO
===

This chapters covers configurations which are relevant for search engine optimization regarding the EXT:tt3_career.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    MetaData
    PageTitle
    RouteEnhancer
    Sitemap
