.. _metadata:

=========
Meta Data
=========

The following metaData can be maintained in a job offer:

- Meta Title
- Meta Description
- Meta Keywords

as well as Open Graph data

for Facebook

- Open Graph Title
- Open Graph Description
- Open Graph Image

and X/Twitter

- Open Graph Title
- Open Graph Description
- Open Graph Image
- Open Graph Card
