.. _sitemap:

=======
Sitemap
=======

In the `[teufels] sitepackage` :file:`/Configuration/TypoScript/Setup/seo.sitemap.typoscript` you find a prepared TypoScript code for creating a sitemap of the job offers.

::

    #include only if hive_career is installed
    jobOffer {
        provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
        config {
            table = tx_hivecareer_domain_model_joboffer
            sortField = sorting
            lastModifiedField = tstamp
            recursive = 1
            pid = 9998 #Speicherort der Beiträge
            url {
                pageId = 9999 #PageID der Detailseite
                fieldToParameterMap {
                    uid = tx_hivecareer_hivecareershow[jobOffer]
                }
                additionalGetParameters {
                    tx_hivecareer_hivecareershow.controller = JobOffer
                    tx_hivecareer_hivecareershow.action = show
                }
                useCacheHash = 1
            }
        }
    }

use this and replace the pid, uid placeholder with the correct ids of your corresponding pages.


