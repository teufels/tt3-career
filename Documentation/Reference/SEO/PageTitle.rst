.. _pageTitle:

=========================
Page title for job detail
=========================

To get a SEO optimized page title it ist recommended to use the extension :ref:`t3brightside/advancedtitle <advancedTitle>`
With the default configuration provided in :file:`/Configuration/TypoScript/Setup/pageTitle.typoscript` the job offer title is used for the page title.

::

    [traverse(request.getQueryParams(), 'tx_tt3career_careershow') && traverse(request.getQueryParams(), 'tx_tt3career_careershow/jobOffer') > 0]
        page.headerData.10.40 = RECORDS
        page.headerData.10.40 {
            dontCheckPid = 1
            tables = tx_tt3career_domain_model_joboffer
            source.data = GP:tx_tt3career_careershow|jobOffer
            source.intval = 1
            conf.tx_tt3career_domain_model_joboffer = TEXT
            conf.tx_tt3career_domain_model_joboffer {
                field = title
                htmlSpecialChars = 1
            }
        }
    [END]


With the general configuration of the :ref:`t3brightside/advancedtitle <advancedTitle>` it is possible to add prefix and suffix as desired.



