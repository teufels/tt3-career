.. _tsGlobalSettings:

Global settings
===============
Any setting needs to be prefixed with `plugin.tx_tt3career.settings`.

.. contents:: Properties
    :depth: 1
    :local:

.. _tsList:

list
----

.. confval:: list

    :type: array
    :Path: plugin.tx_tt3career.settings

    Global configuration for list view like pagination configuration.
    EXT:tt3_career uses the TYPO3 sliding window pagination

    The following settings are available:

    itemsPerPage
        Define how many items are shown on one page.

    maximumLinks
        If set, not all pages of the pagination are shown but only the given amount. Imagine
        1000 job records and 10 items per page. This would result in 100 links in the frontend.

    Default::

        list {
            paginate {
                itemsPerPage = 9
                maximumLinks = 3
            }
        }


.. _tsWatchlist:

watchlist
---------

.. confval:: watchlist

    :type: array
    :Path: plugin.tx_tt3career.settings

    Global configuration for watchlist.

    The following settings are available:

    enable
        Enables or Disables the watchlist function

    cookie-lifetime
        Set the lifetime of the watchlist cookie in days

    Default::

         watchlist {
            enable = true
            cookie-lifetime = 90
        }

.. _tsDetail:

detail
------

.. confval:: detail

    :type: array
    :Path: plugin.tx_tt3career.settings

    Global configuration for detail view.

    The following settings are available:

    relatedJobsTemplate
        defines the template used for the related jobs list inside the detail view.
        could be set to the provided defaulkt templates `list` or `grid` or to a custom template if created.

    Default::

         detail {
            relatedJobsTemplate = grid
        }


.. _tsApplicationForm:

applicationForm
----------------

.. confval:: aoolicationForm

    :type: array
    :Path: plugin.tx_tt3career.settings

    Global configuration for the application form.

    The following settings are available:

    include
        disable or enable the integration of a application form in the detail view

    path
       path to the form which should be used for the include

    Default::

         applicationForm {
            include = false
            path = 1:/form_definitions/applicationForm.form.yaml
        }


.. _tsGoogleForJobs:

googleforjobs
----------------

.. confval:: googleforjobs

    :type: array
    :Path: plugin.tx_tt3career.settings

    Global configuration for Google for jobs.

    The following settings are available:

    employmentType
        Type of employment.
        Choose one or more of the following case-sensitive values:

        - `FULL_TIME`: The job is a full-time position.
        - `PART_TIME`: The job is a part-time position.
        - `CONTRACTOR`: The job is a contractor position.
        - `TEMPORARY``: The job is a temporary position.
        - `INTERN`: The job is an internship position.
        - `VOLUNTEER`: The job is a volunteer position.
        - `PER_DIEM`: The job is paid by the day.
        - `OTHER`: The job is a different type of position that's not covered by the other possible values.

    directapply
       Indicates whether the URL that's associated with this job posting enables direct application for the job.

    identifier
        The hiring organization's unique identifier for the job.
        The following settings are available: name, value

    hiringOrganization
        The organization offering the job position.
        The following settings are available: name, website, logo

    jobLocation.address
        The physical location(s) of the business where the employee will report to work.
        The following settings are available: street, city, region, postalCode, country

    Default::

         googleforjobs {
            employmentType = FULL_TIME

            directapply = 1

            identifier {
                name =
                value =
            }

            hiringOrganization {
                name =
                website =
                logo =
            }

            jobLocation.address {
                street =
                city =
                region =
                postalCode =
                country =
            }
        }

..  tip::
    Check Google Documentation on topic `Job posting <https://developers.google.com/search/docs/appearance/structured-data/job-posting>`__ for further detailed info.
