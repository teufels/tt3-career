.. _typoscript:

==========
TypoScript
==========

.. toctree::
    :maxdepth: 1
    :titlesonly:

    GlobalSettings
    PluginSettings
