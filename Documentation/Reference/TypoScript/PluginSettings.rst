.. _tsPluginSettings:

Plugin settings
===============

This section covers all settings, which can be defined in the plugin itself.
To improve the usability, in the plugin only those settings are shown which are needed by
the chosen view (The setting :confval:`orderBy` is for example not needed in the single view).

.. important::
    Every setting can also be defined by TypoScript setup.
    If set by TypoScript any setting needs to be prefixed with correct plugin path.
    e.g. `plugin.tx_tt3career_careerlist`

..  note::
    Some of the settings are used in multiple plugins.
    This documentation only covers these once.
    Keep in mind that if the setting is in another plugin,
    the prefix path is different depending on the plugin.

.. contents:: Properties
    :depth: 1
    :local:

.. _tsPluginSettingsList:

List Plugin
-----------

PageId for single job offer display `showPid`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: showPid

    :type: int
    :Default: 0 (none)
    :Path: settings.showPid
    :Scope: Plugin, TypoScript Setup
    :required: true

    ::

      plugin.tx_tt3career_careerlist.settings.showPid = 123

   This page is used for showing the job offer detail

Record Storage Page `storagePid`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: storagePid

    :type: string
    :Default: (none)
    :Path: persistence.storagePid
    :Scope: Plugin, TypoScript Setup

    ::

      plugin.tx_tt3career_careerlist.persistence.storagePid = 12,345

    If the storagePid is set, all job offer records which are saved on one
    of the selected pages are shown, otherwise job offers of all pages are shown.

Recursive `recursive`
~~~~~~~~~~~~~~~~~~~~~

.. confval:: recursive

   :type: int
   :Default: 0 (No recursion)
   :Path: persistence.recursive
   :Scope: Plugin, TypoScript Setup

   ::

      plugin.tx_tt3career_careerlist.persistence.recursive = 2

   The search for pages inside storagePid as startingpoint can be extended by setting a recursive
   level.

Max records displayed `limit`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: limit

   :type: int
   :Default: 0 (none)
   :Path: settings.limit
   :Scope: Plugin, TypoScript Setup

   ::

      plugin.tx_tt3career_careerlist.limit = 10

   Define the maximum records shown.

Items per Page `list.paginate.itemsPerPage`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: list.paginate.itemsPerPage

    :type: int
    :Default: 9
    :Path: settings.list.paginate.itemsPerPage
    :Scope: Plugin, TypoScript Setup

    Define the amount of jobOffer items shown per page in the pagination.

Hide the pagination `hidePagination`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: hidePagination

   :type: boolean
   :Default: 1 (hide)
   :Path: settings.hidePagination
   :Scope: Plugin, TypoScript Setup

   If defined, the pagination is not shown.
   Is set to true (hide) on default for update pupose (older version does not have pagination)

Sort by `orderBy`
~~~~~~~~~~~~~~~~

.. confval:: orderBy

    :type: string
    :Default: Manual sorting 'sorting'
    :Path: settings.orderBy
    :Scope: Plugin, TypoScript Setup

    Define the sorting of displayed job offer records.

Sort direction `orderDirection`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: orderDirection

    :type: string
    :Default: Ascending 'ASC'
    :Path: settings.orderDirection
    :Scope: Plugin, TypoScript Setup

    Define the sorting direction which can either be "ASC" for
    ascending or "DESC" descending. This can be either *ASC* or *DESC*.


.. _tsFilter:

Filter `filter`
~~~~~~~~~~~~~~~

.. confval:: filter

    :type: array
    :Default: none
    :Path: settings.filter
    :Scope: Plugin, TypoScript Setup

    Define the filters to be available in frontend.

    Read more about the Filter in :ref:`Filter <filter>`

.. _tsFilter:

.. _tsPreFilter:

Pre-filter `prefilter.*`
~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: prefilter

    :type: array
    :Default: none
    :Path: settings.prefilter.*
    :Scope: Plugin, TypoScript Setup

    Define the values to be pre-filteres
    prefilter settings exist for

    - career level `settings.prefilter.careerlevel`
    - company `settings.prefilter.company`
    - department `settings.prefilter.department`
    - location `settings.prefilter.location`
    - country `settings.prefilter.country`

    Read more about the Filter in :ref:`Filter <filter>`

.. _tsTemplateLayout:

Template Layout `templateLayout`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: templateLayout

   :type: string
   :Default: (none, use default)
   :Path: settings.templateLayout
   :Scope: Plugin, TypoScript Setup

   Select different layouts. Layout `list` and `grid` provided as default Layouts by this extension.
   None is selected to ensure updates from older versions.
   See :ref:`Templating <templatingCreate>` how to add layouts.

Detail Plugin
-------------

Record Storage Page `storagePid`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: storagePid

    :type: string
    :Default: (none)
    :Path: persistence.storagePid
    :Scope: Plugin, TypoScript Setup

    ::

      plugin.tx_tt3career_careerdetail.persistence.storagePid = 12,345

    is required for finding similar jobs.
    If the storagePid is set, all (simular) job offer records which are saved on one
    of the selected pages are shown, otherwise (simular) job offers of all pages are shown.

Recursive `recursive`
~~~~~~~~~~~~~~~~~~~~~

.. confval:: recursive

   :type: int
   :Default: 0 (No recursion)
   :Path: persistence.recursive
   :Scope: Plugin, TypoScript Setup

   ::

      plugin.tx_tt3career_careerdetail.persistence.recursive = 2

   The search for pages inside storagePid as startingpoint can be extended by setting a recursive
   level.

Max similar records displayed `limit`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: limit

   :type: int
   :Default: 0 (none = no records listed)
   :Path: settings.limit
   :Scope: Plugin, TypoScript Setup

   ::

      plugin.tx_tt3career_careerdetail.limit = 10

   Define the maximum number of similar records listed (empty = no records listed)

Template Layout `templateLayout`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: templateLayout

   :type: string
   :Default: (none, use default)
   :Path: settings.templateLayout
   :Scope: Plugin, TypoScript Setup

   Select different layouts.
   See :ref:`Templating <templatingCreate>` how to add layouts.

Watchlist Plugin
-----------------

PageId for job offer list  `listPid`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: listPid

    :type: int
    :Default: 0 (none)
    :Path: settings.showPid
    :Scope: Plugin, TypoScript Setup


   Optional adds back to all job offers button to the selected page

..  seealso::
    For other settings see :ref:`Settings for list Plugin <_tsPluginSettingsList>`

Watchlist Button Plugin
-----------------------

PageId for watchlist `watchlistPid`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. confval:: watchlistPid

    :type: int
    :Default: 0 (none)
    :Path: settings.watchlistPid
    :Scope: Plugin, TypoScript Setup
    :required: true

    ::

      plugin.tx_tt3career_careerwatchlistbutton.settings.watchlistPid = 123

    This page is used as target for the watchlist burron and should contain the page where the watchlist plugin is placed.
