..  include:: /Includes.rst.txt

..  _introduction:

============
Introduction
============

.. contents:: Table of contents
    :depth: 1
    :local:

What does it do?
================

This extension allows you to create and manage job listings, featuring filtering, watchlist,
detail view with optional application form or link to external one, and compatibility with Google for Jobs structured data.

User can apply to a job offer by using the supplied application form resulting in the referenced contact receiving the application via email
and the application get logged in form_log, making a simple application tool.

..  _features:

Features
========

* list view and detail page
* comes with pre-applied application form integrated in the detail page which supports multi-file-upload
* use **frontend filters** like search word (title, subtitle, keywords), employment type, career level, company, department, location, country
* Use **predefined filers** for pre-filtering
* Related Jobs list shows on Detail Page based on matching attributes
* Use Sliding window pagination
* job offers could be stored and viewed in a watchlist over a watchlist-button
* select in the plugin which filters are usable in the frontend and in which order they are displayed
* create contact person records to link with the job offer
* shipped with a bootstrap layout, which provides easy customizability
* comes with a simple styling so it **works out of the box**
* list plugin comes with 2 template layouts (list and grid) which can be selected in the plugin
* own template layouts could be created
* add header images for detail page
* support for **TYPO3 categories**
* Support OpenGraph data
* Structured data for **Google Jobs integration**

..  _screenshots:

Screenshots
===========

..  figure:: /Images/Introduction/screenshot_list_list.png
    :class: with-shadow
    :alt: Screenshot of listing (list layout)
    :width: 600px

    Screenshot of the listing (list layout)

..  figure:: /Images/Introduction/screenshot_list_grid.png
    :class: with-shadow
    :alt: Screenshot of listing (grid layout)
    :width: 600px

    Screenshot of the listing (grid layout)

..  figure:: /Images/Introduction/screenshot_detail.png
    :class: with-shadow
    :alt: Screenshot of the detail page
    :width: 600px

    Screenshot of the detail page

..  figure:: /Images/ApplicationForm/screenshot_applicationform.png
    :class: with-shadow
    :alt: Screenshot of the application form
    :width: 600px

    Screenshot of the application form

..  figure:: /Images/Introduction/screenshot_watchlist.png
    :class: with-shadow
    :alt: Screenshot of the application form
    :width: 600px

    Screenshot of the watchlist with the watchlist-button

| This is how the standard frontend will look like.
| Of course you can easily override these templates via typoscript.
