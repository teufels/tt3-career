..  include:: /Includes.rst.txt

..  _administration:

===========
Administration
===========

This chapter describes how to manage the extension from a superuser point of view.

.. toctree::
    :maxdepth: 3
    :titlesonly:
    :glob:

    Installation
    Configuration
    ApplicationForm/Index
    Maintenance
