..  _applicationForm-general:

==========
Setup form
==========

.. contents:: Table of contents
    :depth: 1
    :local:


Usage of application form
-------------------------
the extension can implement the provided application form in the detail view.
By default this is turned off but could be changed in the TypoScript settings:

..  code-block:: typoscript
    :caption: TypoScript constants

    plugin.tx_tt3career.settings {
      applicationForm {
         # configure include of application form in detail
         include = true
      }
    }

Read more about configurations via TypoScript in the
:ref:`Reference/TypoScript <typoscript>` section.


Create application form
-----------------------
this extension provides a predefined form

..  figure:: /Images/ApplicationForm/screenshot_applicationform.png
    :class: with-shadow
    :alt: Screenshot of the application form
    :width: 600px

    Screenshot of the default application form

to create this form, follow these **steps**:

..  rst-class:: bignums

1.  Under :guilabel:`Forms` create a new form
2.  Select :guilabel:`Predefined Form`
3.  Set the settings as follows:

    * Template: :guilabel:`Basic application form (ext:tt3_career)`
    * Name: :guilabel:`Application Form`
    ..  figure:: /Images/ApplicationForm/create_predefined_applicationform.png
        :class: with-shadow
        :alt: Screenshot of the application form
        :width: 400px

        Screenshot showing the creation of the predefined application form

containing the following fields:

..  confval:: first name
    :identifier: firstname
    :required: true
    :type: Text
    :validators: NotEmpty

..  confval:: last name
    :identifier: lastname
    :required: true
    :type: Text
    :validators: NotEmpty

..  confval:: email
    :identifier: email
    :required: true
    :type: Text
    :validators: NotEmpty, EmailAddress

..  confval:: phone number
    :identifier: telephone
    :required: false
    :type: Telephone
    :validators: none

..  confval:: File upload
    :identifier: fileupload-application
    :required: false
    :type: FileUpload
    :validators: FileSize (0 - 10 MB)
    :multiple: true
    :allowedMimeTypes: application/msword,
        application/vnd.openxmlformats-officedocument.wordprocessingml.document,
        application/pdf,
        image/jpeg,
        image/png,
        image/bmp
    :saveToFileMount: 2:/application_form_upload/

..  confval:: terms
    :identifier: terms-text
    :type: LinkedText
    :poperties: pageUid: 7

..  confval:: job
    :identifier: job
    :type: Text
    :hidden: true

    filled in automatically

..  confval:: receiver name
    :identifier: receiver-name
    :type: Text
    :hidden: true
    :variants: hide-in-mail

    filled in automatically

..  confval:: receiver mail
    :identifier: receiver-mail
    :type: Text
    :hidden: true
    :variants: hide-in-mail

    filled in automatically

..  confval:: additional receiver
    :identifier: receiver-additional
    :type: Text
    :hidden: true
    :variants: hide-in-mail

    filled in automatically

and the following finishers:

..  confval:: Email to receiver (you)
    :identifier: EmailToReceiver
    :type: Finisher
    :templateName: EmailToReceiver
    :translationFiles: EXT:sitepackage/Resources/Private/Language/Form/locallang_form_finisher.xlf, EXT:tt3_career/Resources/Private/Language/Form/locallang_form_finisher.xlf'

    change recipients on Form configuration in the TYPO3 Backend

    Mail text is set in translation files

..  confval:: Confirmation massage
    :identifier: Confirmation
    :type: Finisher
    :translationFiles: EXT:sitepackage/Resources/Private/Language/Form/locallang_form_finisher.xlf, EXT:tt3_career/Resources/Private/Language/Form/locallang_form_finisher.xlf'

    Confirmation text is set in translation files

..  confval:: Log form data
    :identifier: LogFormData
    :type: Finisher

    logging of form data over the :ref:`EXT:formlog<FormLog>`


Configure application form
--------------------------

..  rst-class:: bignums

1.  Enter the sender’s email address in the Finisher "Email to Receiver (you) > Sender address"
    - Optional: Sender Name in Finisher “Email to Receiver (you) > Sender name” pflegen
2.  Maintain the recipient email address(es) in the Finisher “Email to Receiver (you) > Recipients”
    By default, the recipient is set as {receiver-mail} => {receiver-name}, which is pulled from the contact person for the job.
    - Optional: Additional (global) recipients from the client can also be added to Recipients, CC, or BCC as needed.
     This must also be set if job offer records exist without a contact person.
    - Optional: Add email for debugging in BCC.
3.  All texts are managed via translation files here:
    :file:`/tt3_career/Resources/Private/Language/Form`
    :file:`/sitepackage/Resources/Private/Language/Form`
4.  Extending or overriding the texts should only be done through the sitepackage.
    Never change the translation files in the extension directly!


..  note::
    Recipient from Job Object
    {receiver-mail} => {receiver-name} is set based on the contact person selected for the job position and can be used in the Finisher settings as needed.

..  note::
    Additional Recipients from Job Object
    The “Additional receivers” from the job position, if available, are appended to the BCC recipients.


Include TypoScript template
---------------------------
For the predefined application form it is necessary to include some TypoScript templates.

..  rst-class:: bignums

1. Switch to the root page of your site.

2. Switch to :guilabel:`Template > Info/Modify`.

3. Press the link :guilabel:`Edit the whole template record` and switch to the tab :guilabel:`Includes`.

4. add the following templates from the list to the right above the :guilabel:`[teufels] Career (tt3_carrer)`:

    - :guilabel:`Form Extended (form_extended)`.
    - :guilabel:`Linked text configuration (form_element_linked_text)` if not already included in Page or ROOT TypoScript.

Read more about possible configurations via TypoScript in the
:ref:`Reference/TypoScript <typoscript>` section.


Process
-------

1. Display summary page before submitting the data.
2. Display confirmation message.
3. Data is sent to the defined recipients (from the contact person and additional addresses maintained in the form).
4. Uploaded files are sent as attachments.
5. Data is additionally stored in the database (Form log) as a backup in case there are issues with email delivery/receipt.
6. This data is automatically deleted after 14 days (default setting).
   For more details, see information for the data protection declaration :ref:`Data protection <applicationForm-dataProtection>`.
7. Uploads are stored (in addition to being attached to the email) in form-specific folders (with cryptic names) outside the website root, and therefore cannot be accessed directly via the frontend. Data access via the backend (optional frontend) is restricted to a specific user group.
8. These files are automatically deleted after 14 days (default setting).
9. For more details, see information for the data protection declaration :ref:`Data protection <applicationForm-dataProtection>`.


Custom application form
-----------------------
this extension provides a predefined form :file:`EXT:tt3_career/Resources/Private/Form/BasicApplication.form.yaml` which could be used.
Own form could be used by changing `applicationForm.path` in TypoScript settings.

..  code-block:: typoscript
    :caption: TypoScript constants

    plugin.tx_tt3career.settings {
      applicationForm {
         path = 1:/form_definitions/applicationFormCustom.form.yaml
      }
    }

Read more about configurations via TypoScript in the
:ref:`Reference/TypoScript <typoscript>` section.

