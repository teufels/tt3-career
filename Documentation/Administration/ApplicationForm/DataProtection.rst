..  _applicationForm-dataProtection:

===============
Data Protection
===============

..  attention::
    It is strongly recommended to take the following data protection measures

..  rst-class:: bignums

1. Create a Typo3 scheduler task in TYPO3 Backend (if not already exist)
   for the automated deletion of data from form_log after x days. (14 day recommended) ::

    Task: Table garbage collection (scheduler)
    Task group: Data protection *create if not exist
    Type: Recurring
    Frequency: Every night (e.g., 0 4 * * *)
    Table to clean up: tx_formlog_entries
    Delete entries older than the given number of days: 14

2.  Create a Typo3 scheduler task in TYPO3 Backend (if not already exist)
    for the automated deletion of upload files after x days (14 days recommended) ::

     Task: Delete old files (hive_scheduler)
     Task group: Data protection *create if not exist
     Type: Recurring
     Frequency: Every night (e.g., 15 4 * * *)
     Path: <absolut_path>/secure_fileadmin/application_form_upload
     Minimum age of the deleted files/directories: 14 days
     What to delete?: All old files and directories (checks age of all files and directories recursively)
     Advanced Mode: On

3.  Set up the Typo3 scheduler as
    cron job that runs every 5/10/15/30 minutes daily on the server ::

     /opt/php-8.2/bin/php /var/www/clients/client123/web123/web/ws/24/production/current/web/typo3/sysext/core/bin/typo3 scheduler:run


.. warning::
    The fields, required fields, and finisher (workflow) were coordinated with a data protection officer. Based on their recommendation, the provided form includes only a notice about "processing in accordance with data protection" and does not use a checkbox for consent.

.. warning::
    Additional fields, upload options, file types, and required fields should be created or defined individually for each project, always in consultation with the client's data protection officer. When adding new (upload) fields, it must be ensured that they are GDPR-compliant (e.g., automatic deletion after a certain number of days).
    It is also important to include new fields in the translation files, such as labels, descriptions, etc.

.. warning::
    There should always be coordination with the client's data protection officer, and the form should be built according to their recommendations. This especially applies to changes in fields, workflow, and finishers.


Email delivery
--------------
The Fluid mail template is used for sending emails.
It is strongly recommended to ensure data protection compliance by using SMTP with at least TLS 1.2 for email transmission.
The sending and receiving of emails should be thoroughly tested.
