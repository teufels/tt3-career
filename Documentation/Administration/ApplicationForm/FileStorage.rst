..  _fileStorage:

============
File Storage
============

..  attention::
    For security and data protection reasons, it is strongly recommended to set up a file storage
    that must be browsable and writable, but not publicly accessible.
    See more about file storage `here <https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/Fal/Administration/Storages.html>`__

.. contents:: Table of contents
    :depth: 1
    :local:

Create secure file storage
--------------------------
1. create an additional folder :guilabel:`secure_fileadmin` besides :guilabel:`fileadmin`.
create appropriate symlinks and include the new folder in pipeline processes if needed

2. create new File Storage in the :guilabel:`Root [0]` with following settings:
- :guilabel:`General > Name`:  :guilabel:`secure_fileadmin`.
- :guilabel:`General > Base path`: absolut path to the previously created :guilabel:`secure_fileadmin` folder.
e.g.: `/var/www/[...]/web/ws/24/staging/shared/secure_fileadmin/`
  (on go LIVE path needs to be set to `/var/www/[...]/web/ws/24/production/shared/secure_fileadmin/`)
- :guilabel:`General > Path type`:  :guilabel:`absolute`.
- :guilabel:`Access capabilities > Is publicly available?`:  :guilabel:`off[0]`.

Create folder for application form uploads
------------------------------------------
#. create subfolder :guilabel:`application_form_upload` within the previously created :guilabel:`secure_fileadmin` folder.

#. create new Filemount in the :guilabel:`Root [0]` with following settings:
- :guilabel:`General > Label`:  :guilabel:`secure_fileadmin/application_form_upload/`.
- :guilabel:`General > Storage`:  :guilabel:`secure_fileadmin/`.
- :guilabel:`General > Folder`:  :guilabel:`/application_form_upload/`.
- :guilabel:`General > Read-only?`:  :guilabel:`off[0]`.

.. note::
   Through these two measures, access is only possible as a BE user with the corresponding right to access Secure Storage or via FTP/SSH

.. warning::
   Starting from Typo3 11.5.35, the absolute URL to secure_fileadmin must be entered into the
   lookRootPath variable (preferably by adjusting the AdditionalConfiguration.php and including
   the path in the staging.ini/production.ini).

(Optional) grante access for FE users
-------------------------------------
#. install & configurate extension FAL Secure Download :ref:`<https://extensions.typo3.org/extension/fal_securedownloadd>` according to instructions.

#. create FE-User group :guilabel:`application_form_upload` over the list-view inside the created :guilabel:`Career` folder

#. Set folder permissions on the :guilabel:`/application_form_upload/` folder and select the :guilabel:`application_form_upload` group.
..  figure:: /Images/ApplicationForm/set_folderpermission.png
    :class: with-shadow
    :alt: Screenshot of the folder configuration
    :width: 600px

    Screenshot of the permission configuration for the folder application_form_upload

.. note::
   This measure allows access as a logged-in frontend (FE) user with the "application_form_upload" group. If needed, a possible FE login and file listing still need to be implemented.

.. note::
   Detailed instructions can be found at https://jweiland.net/typo3-cms/showcase/fal-securedownload.html

