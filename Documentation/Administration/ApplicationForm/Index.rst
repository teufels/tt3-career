..  include:: /Includes.rst.txt
..  _applicationForm:

================
Application form
================

This chapter describes how to create, manage and use the application form.

..  toctree::
    :maxdepth: 5
    :titlesonly:

    General
    FileStorage
    DataProtection
