.. _installation:

============
Installation
============

In a :ref:`composer-based TYPO3 installation <t3start:install>` you can install
the extension EXT:tt3_career via composer:

.. code-block:: bash

   composer require teufels/tt3-career


Update the database scheme
--------------------------

Open your TYPO3 backend with :ref:`system maintainer <t3start:system-maintainer>`
permissions.

In the module menu to the left navigate to :guilabel:`Admin Tools > Maintanance`,
then click on :guilabel:`Analyze database` and create all.

.. figure:: /Images/Configuration/AnalyzeDatabase.png
   :class: with-shadow


Clear all caches
----------------

In the same module :guilabel:`Admin Tools > Maintanance` you can also
conveniently clear all caches by clicking the button :guilabel:`Flush cache`.

.. figure:: /Images/Configuration/FlushCache.png
   :class: with-shadow
