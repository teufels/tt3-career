.. _configuration:

=============
Configuration
=============

Include TypoScript template
===========================

It is necessary to include at least the basic TypoScript provided by this
extension.

Go module :guilabel:`Web > Template` and chose your root page.
It should already contain the Sitepackage TypoScript template record.
Switch to view
:guilabel:`Info/Modify` and click on :guilabel:`Edit the whole template record`.

.. figure:: /Images/Configuration/Tt3CareerIncludeTypoScript.png
:class: with-shadow

Switch to tab :guilabel:`Includes` and add the following templates from the list
to the right:
- :guilabel:`Linked text configuration (form_element_linked_text)`. (when using application form and not already included)
- :guilabel:`Form Extended (form_extended)`. (when using application form)
- :guilabel:`[teufels] Career (tt3_carrer)`.

add them above the :guilabel:`[teufels] Sitepackage (sitepackage)`.

Read more about configurations via TypoScript in the
:ref:`Reference <typoscript>` section.


TypoScript Settings
===================

All necessary settings, except for the necessary global settings for "Google for jobs",
can be made via the plugins.

The settings for "Google for jobs" can be found under :ref:`TypoScript/Global settings<tsGoogleForJobs>`

All possible TypoScript settings can be found under :ref:`Reference/TypoScript <typoscript>`

