CREATE TABLE tx_tt3career_domain_model_joboffer (
	backend_title varchar(255) NOT NULL DEFAULT '',
	header_image int(11) unsigned NOT NULL DEFAULT '0',
	header_text text,
	title varchar(255) NOT NULL DEFAULT '',
	subtitle varchar(255) NOT NULL DEFAULT '',
	start_date date DEFAULT NULL,
	employmenttype varchar(255) NOT NULL DEFAULT '',
    career_level varchar(255) NOT NULL DEFAULT '',
    department varchar(255) NOT NULL DEFAULT '',
	company varchar(255) NOT NULL DEFAULT '',
	location varchar(255) NOT NULL DEFAULT '',
    additional_locations varchar(255) NOT NULL DEFAULT '',
    country varchar(255) NOT NULL DEFAULT '',
	intro text,
	task text,
	profile text,
    benefits text,
	information text,
	download int(11) unsigned NOT NULL DEFAULT '0',
	external_link varchar(255) NOT NULL DEFAULT '',
	meta_title varchar(255) NOT NULL DEFAULT '',
	meta_description text NOT NULL DEFAULT '',
	meta_keywords text NOT NULL DEFAULT '',
	og_title varchar(255) NOT NULL DEFAULT '',
	og_description text NOT NULL DEFAULT '',
	og_image int(11) unsigned NOT NULL DEFAULT '0',
	twitter_title varchar(255) NOT NULL DEFAULT '',
	twitter_description text NOT NULL DEFAULT '',
	twitter_image int(11) unsigned NOT NULL DEFAULT '0',
	twitter_card varchar(255) NOT NULL DEFAULT '',
	googleforjobs int(11) unsigned NOT NULL DEFAULT '0',
	contact int(11) unsigned DEFAULT '0',
    additional_receiver varchar(255) NOT NULL DEFAULT '',
    path_segment varchar(2048),
    categories int(11) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tx_tt3career_domain_model_googleforjobs (
	joboffer int(11) unsigned DEFAULT '0' NOT NULL,
	backend_title varchar(255) NOT NULL DEFAULT '',
	title varchar(255) NOT NULL DEFAULT '',
	subtitle varchar(255) NOT NULL DEFAULT '',
	description text,
	employmenttype varchar(255) NOT NULL DEFAULT '',
	identifier_name varchar(255) NOT NULL DEFAULT '',
	identifier_value varchar(255) NOT NULL DEFAULT '',
	hiringorganization_name varchar(255) NOT NULL DEFAULT '',
	hiringorganization_website varchar(255) NOT NULL DEFAULT '',
	hiringorganization_logo int(11) unsigned NOT NULL DEFAULT '0',
	joblocation_address_street varchar(255) NOT NULL DEFAULT '',
	joblocation_address_postalcode varchar(255) NOT NULL DEFAULT '',
	joblocation_address_city varchar(255) NOT NULL DEFAULT '',
	joblocation_address_region varchar(255) NOT NULL DEFAULT '',
	joblocation_address_country varchar(255) NOT NULL DEFAULT '',
	basesalary_currency varchar(255) NOT NULL DEFAULT '',
	basesalary_unit varchar(255) NOT NULL DEFAULT '',
	basesalary_value double(11,2) NOT NULL DEFAULT '0.00',
	remotejob smallint(1) unsigned NOT NULL DEFAULT '0',
	remotejob_location_requirements varchar(255) NOT NULL DEFAULT '',
	date_posted date DEFAULT NULL,
	valid_through date DEFAULT NULL
);
